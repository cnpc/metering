module VerificationHelper
  def etalon_indicating_values
    %w(0 0.6 1.2 1.8 2.4 3 3.6 4.2 4.8 5.4 6)
  end
  def style_next_check_time(date)
    return "" if date.blank?
    date += 1.year
    if date > (Date.today + 1.month)
      ""
    elsif date > Date.today
      "#fee188"
    else
      "#d15b47"
    end
  end
end
