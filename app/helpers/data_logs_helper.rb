module DataLogsHelper
  def format_diff(content)
    options = JSON.parse(content)
    result = ""
    options.each do |option|
      case option.first
      when "-"
        result += "<p>删除 #{option[1]} ，其值为#{option[2]}</p>"
      when "+"
        result += "<p>新增 #{option[1]} ，其值为#{option[2]}</p>"
      when "~"
        result += "<p>变更 #{option[1]}，由 #{option[2]} 变更为 #{option[3].present? ? option[3] : "空"}</p>"
      end
    end
    result.html_safe
  end
end
