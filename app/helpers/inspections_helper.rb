module InspectionsHelper
  def inspection_category
    %w(计量巡检任务书 计量巡检报告 计量巡检问题汇总表 计量巡检问题整改汇总表)
  end
end
