class Notice < Sequel::Model
  plugin :modification_detection

  many_to_one :instrument
  many_to_one :organ
  many_to_one :verification
  one_to_many :data_logs
  one_to_many :notifications

  class << self
    def default_data notice
      record = File.read("lib/certificates/#{Settings.records.send("#{notice.instrument.instrument_kind.name}")}")
      self.instance_eval(record)
    end

    def check_expired_and_create_notification
      unexpired_notices.each do |notice|
        result = notice.judge_expiry_date
        if result
          notice.organ.users.each do |user|
            Notification.create(notice_id: notice.id, organ_id: notice.organ.id, user_id: user.id, content: result)
          end
        end
      end
    end

    def unexpired_notices
      self.eager(:organ).where(expired: false)
    end
  end

  def judge_expiry_date
    case expiry_date
    when Date.today
      "1天"
    when Date.today + 10.days
      "10天"
    when Date.today + 30.days
      "30天"
    else
      false
    end
  end

  def should_notify?(notify_duration)
    expiry_date == Date.today + notify_duration.days #即将过期
  end

  ["notice","certificate","calibration"].each_with_index do |type,index|
    define_method("is_#{type}?") do
      self.certificate == index ? true : false
    end
  end
end
