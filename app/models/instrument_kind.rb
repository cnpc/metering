# -*- coding: utf-8 -*-
# 仪器种类
class InstrumentKind < Sequel::Model(:instrument_kinds)
  plugin :validation_helpers
  plugin :active_model

  # one_to_one :category_type, class_name: "CategoryType"
  many_to_one :category,class_name: "Category" # 检测分类

  one_to_many :regulations, class_name: "Regulation"

  def validate
    super
    validates_presence [:category_id, :name, :expries_date]
  end

  # def after_create
  # 	super
  # 	self.category_type = CategoryType.new(name: self.name,category_id: self.category_id)
  # 	self.category_type.save
  # end
end