class Settings < Settingslogic
  source "#{Rails.root}/config/application.yml"
  namespace Rails.env
  Settings.reload! if Rails.env.development?
end
