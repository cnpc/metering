# -*- coding: utf-8 -*-
# 检测精度
class CategoryPrecision < Sequel::Model(:category_precisions)
  plugin :validation_helpers
  plugin :active_model
  many_to_one :category,class_name: "Category"
  def validate
    super
    #validates_unique [:name]
  end

  def precision
    "#{self.try(:name)}级"
  end
end
