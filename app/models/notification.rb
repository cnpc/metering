class Notification < Sequel::Model
  many_to_one :user
  many_to_one :organ
  many_to_one :notice

  def_dataset_method(:unchecked) do
    filter(checked: false).order(:created_at).reverse
  end
end
