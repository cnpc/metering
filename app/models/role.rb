class Role < Sequel::Model
    one_to_many :users

    def self.admin
        Role.find(name: 'admin')
    end


    def self.manager
        Role.find(name: 'management')
    end


    def self.common
        Role.find(name: 'common')
    end


    def i18n
        keys = {admin: "系统管理员", management: "领导", common: "检定员/核验员"}
        keys[self.name.to_sym]
    end
end
