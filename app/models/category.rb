# -*- coding: utf-8 -*-
# 检测分类
class Category < Sequel::Model(:categories)
  plugin :validation_helpers
  plugin :active_model

  # one_to_many :category_precisions,class_name: "CategoryPrecision"
  # one_to_many :category_types,class_name: "CategoryType"
  # one_to_many :category_ranges,class_name: "CategoryRange"
  one_to_many :instrument_kinds, class_name: "InstrumentKind"
  one_to_many :vendors, class_name: "Vendor"
  one_to_many :regulations, class_name: "Regulation"

  def validate
      super
      #validates_unique [:name]
    end
end
