# -*- coding: utf-8 -*-
module DateValidate
  def self.expires instruments
    instruments.map{|instrument|
    	date = instrument.verifications.last.date
    	expires_day = instrument.instrument_kind.expries_date
    	expries_date = date+(expires_day.days rescue 0.days)
    	result = expires?(date,expires_day)
    	if result[0]
    		build_data(instrument).merge({expires_notify: result[1],expries_date: expries_date})
    	end
    }.compact
  end

  def self.scraps instruments
    instruments.map{|instrument|
    	date = instrument.verifications.last.date
    	expires_day = instrument.instrument_kind.expries_date
    	expries_date = date+(expires_day.days rescue 0.days)
    	if scraps?(date,expires_day)
    		build_data(instrument).merge({expries_date: expries_date})
    	end
    }.compact
  end

  private
  def self.scraps? default_date,expires_days
  	expires_date = default_date + (expires_days.days rescue 0.days)
    current_date = Date.parse(Time.now.to_s)
    feature_date = Date.parse((expires_date+2.year).to_s)
    current_date > feature_date
  end

  def self.expires? default_date,expires_days
    expires_date = default_date + (expires_days.days rescue 0.days)
    current_date = Date.parse(Time.now.to_s)
    feature_date = Date.parse((expires_date+2.year).to_s)
    return [false,nil] if current_date < default_date
    return [false,nil] if current_date > feature_date
    return [true,"已到期"] if current_date > expires_date 
    days = (expires_date - current_date).to_i
    return [true,"还有#{days}天将到期"] if days <= 30
    [false,nil]
  end

  def self.build_data instrument
  	{instrument_kind: instrument.instrument_kind.name,
    			sn: instrument.sn,
    			specification: instrument.specification,
    			date: instrument.verifications.last.date.to_s}
  end
end