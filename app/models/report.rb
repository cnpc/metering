# -*- coding: utf-8 -*-
# 检定纪录报告
class Report < Sequel::Model(:reports)
  one_to_one :verification,class_name: "Verification" # 检定申请纪录

  # report == Verification
  # record == report
  # Examine::RecordRepo => Report
  # Examine::RecordDomain => Report
  class << self
    def default_data verification
      @report = verification
      record = File.read("lib/records/#{Settings.records.send("#{verification.instrument.instrument_kind.name}")}")
      self.instance_eval(record)
    end

    def verify verification, args = {}
      @record = args.empty? ? Report.default_data(verification) : args
      byebug
      # verification.update(standard_id: args['标准器']) if args['标准器']

      record_inst = Report.first(verification_id: verification.id)
      unless record_inst
        record_inst = Report.create(verification_id: verification.id, data: @record.to_json)
      else
        record_inst = record_inst.update(data: @record.to_json)
      end

      calculate = File.read("lib/calculates/#{Settings.calculates.send("#{verification.instrument.instrument_kind.name}")}")
      calculates  = self.instance_eval(calculate)

      record_inst = Report.first(verification_id: verification.id)
      unless record_inst
        record_inst = Report.create(verification_id: verification.id, data: calculates.to_json)
      else
        record_inst.update(data: calculates.to_json)
      end

      @facts = calculates["计算结果"]
      rule   = File.read("lib/rules/#{Settings.rules.send("#{verification.instrument.instrument_kind.name}")}")
      valid  = self.instance_eval(rule)

      # 将验证结果存到 data 中
      record_inst.update(data: calculates.merge({result: valid}).to_json)

      JSON.parse(record_inst.data)
    end
  end

  def record
    self.data
  end
end
