# -*- coding: utf-8 -*-
class Verification < Sequel::Model(:verifications)
  plugin :validation_helpers
  plugin :active_model
  many_to_one :instrument,class_name: "Instrument", key: 'instrument_id'
  one_to_one :report,class_name: "Report"
  many_to_one :organ,class_name: "Organ"
  many_to_one :standard, class_name: "Instrument", key: "standard_id"
  one_to_many :notices
  many_to_one :user

  def validate
    super
    validates_presence [:instrument_id, :organ_id, :date], message: '不能为空'
  end

 class << self
    %w|registered examined verified|.each do |status|
      define_method "#{status}" do
        Verification.where("state = ?", status)
      end
    end
  end

  def notice
    self.notices_dataset.where(expired: false).last
  end

  def generate_notice
    notice = Notice.create(verification_id: self.id, instrument_id: self.instrument_id, organ_id: self.instrument.organ_id)
    mark_old_notice_as_expired(notice)
    save_notice_content(notice)
  end

  def mark_old_notice_as_expired(notice)
    self.instrument.notices_dataset.where("id != ? AND expired = ?", notice.id, false).update(expired: true)
  end

  def save_notice_content(notice)
    data = self.report.data.blank? ? Report.default_data(self) : JSON.parse(self.report.data)
    notice.certificate = data['qualified'] ? 1 : 0
    if data['计算结果'.force_encoding('utf-8')]
      content = data['计算结果'.force_encoding('utf-8')].merge(data['result']).merge(qualified: data['qualified'])
      notice.content = content.to_json
    end
    notice.save
  end

  def after_create
    super
    report = Report.create(verification_id: self.id)
    self.update(report_id: report.id)
  end

  state_machine :state, initial: :registered do

    event :examined do
      transition [:registered] => :examined
    end

    event :verified do
      transition [:examined] => :verified
    end

    event :approved do
      transition [:verified] => :approved
    end
  end
end
