class Organ < Sequel::Model
  one_to_many :users
  one_to_many :instruments
  one_to_many :verifications
  one_to_many :notices
  one_to_many :notifications
end
