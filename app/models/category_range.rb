# -*- coding: utf-8 -*-
# 检测范围

class CategoryRange < Sequel::Model(:category_ranges)
  plugin :validation_helpers
  plugin :active_model
  many_to_one :category,class_name: "Category"
  def validate
      super
      #validates_unique [:name]
  end

  def range
  	return "(#{self.start}~#{self.end}) #{self.unit}"
  end

end
