class Inspection < Sequel::Model
  plugin :validation_helpers
  plugin :timestamps, :force=>true, :update_on_create=>true

  mount_uploader :attachment, AttachmentUploader

  def validate
    super
    validates_presence [:title, :date, :attachment], message: '不能为空'
  end
end
