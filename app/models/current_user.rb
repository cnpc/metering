class CurrentUser

  attr_accessor :username, :id, :abilities

  def initialize
    @abilities = []
  end

  def roles_included? roles
    Array(roles).include?(username)
  end


  def organ_id
    User[id].organ_id
  end

  def common?
    User[self.id].common?
  end

  def admin?
    User[self.id].admin?
  end

  def management?
    User[self.id].management?
  end
end
