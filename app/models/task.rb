# -*- coding: utf-8 -*-

class Task < Sequel::Model
  state_machine :state, initial: :todo do
    event :finish do
      transition [:todo] => :finished
    end
  end
end