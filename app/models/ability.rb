class Ability
  include CanCan::Ability

  def initialize(user)
    #common
    alias_action :read, :create, :organ, :update, :certificate, :report, :to => :apply
    alias_action :read, :create, :update, :search, :to => :cru
    #management
    alias_action :calculate, :update_state, :to => :process

    can :search, Notice
    can [:expires, :scraps], Instrument

    #can :read, Instrument
    if user.common?
      can [:process, :apply], Verification
      can :cru, Instrument
      can :cru, Vendor
      can :read, Notification
      can [:read,:organ], Setting
      can :show, User
      can :read, Notice
      can :read, Inspection
    end

    if user.management?
      can :cru, Instrument
      can [:read, :create, :update, :edit_inside_page, :update_content], Notice
      can [:process, :apply], Verification
      can :cru, Vendor
      can [:read,:organ], Setting
      can [:read,:update], User
      can :cru, Regulation
      can :read, Notification
      can :read, Inspection
      can :read, DataLog
      can [:read, :create, :update], InstrumentKind
    end
    if user.admin?
      can :cru, Organ
      can :manage,:all
    end
    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
  end
end
