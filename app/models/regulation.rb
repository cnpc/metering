class Regulation < Sequel::Model
  one_to_many :instruments,class_name: "Instrument"

  many_to_one :instrument_kind, class_name: "InstrumentKind"
end
