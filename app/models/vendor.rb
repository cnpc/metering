# -*- coding: utf-8 -*-
# 生产厂商
class Vendor < Sequel::Model(:vendors)
  plugin :validation_helpers
  plugin :active_model

  many_to_one :category, class_name: "Category"
  def validate
      super
      validates_unique [:name], message: '该名称已经存在'
      validates_presence [:name], message: '名称不能为空'
    end
end
