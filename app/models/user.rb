class User < Sequel::Model
  attr_accessor :login

  plugin :validation_helpers
  # plugin :devise
  plugin :timestamps, :update_on_create => true
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable, :recoverable
  #devise :database_authenticatable, :registerable,
         #:rememberable, :trackable, :validatable, :recoverable
  # devise :database_authenticatable, :validatable

  many_to_one :role
  many_to_one :organ
  one_to_many :data_logs
  one_to_many :notifications
  one_to_many :verifications



  # ----- 状态机 ------
  state_machine :state, :initial => :unknown do
    event :register do
      transition [:unknown] => :unauthorized
    end

    event :approve do
      transition [:unknown, :unauthorized] => :approved
    end
  end



  def validate
    super
    errors.add(:username, '已经存在') if username && new? && User[:username=>username]
    # validates_unique :email, message: '已经存在'
    # validates_presence [:username, :email, :organ_id, :role_id], message: '不能为空'
    validates_presence [:username], message: '不能为空'
  end

  def login=(login)
    @login = login
  end

  def login
    @login || self.username || self.email
  end


  def state_human
    keys = {unknown: "未注册", unauthorized: "待批准", approved: "正常"}
    keys[self.state.to_sym] unless self.state.nil?
  end


  def self.find_for_database_authentication(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions.to_hash).where(["lower(username) = :value OR lower(email) = :value", { :value => login.downcase }]).first
    else
      where(conditions.to_hash).first
    end
  end

  ["common","management","admin"].each do |role_name|
    define_method("#{role_name}?") do
      return false if self.role.nil?
      self.role.name == role_name ? true : false
    end
  end
end
