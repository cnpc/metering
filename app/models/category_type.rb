# -*- coding: utf-8 -*-
#  检测类型
class CategoryType < Sequel::Model(:category_types)
  plugin :validation_helpers
  plugin :active_model
  many_to_one :category,class_name: "Category"

  def self.with_report
    permit_calculation = %w(数字压力计 弹性元件式一般压力表 常用玻璃量器 砝码 架盘天平 电子天平)
    self.where(name: permit_calculation)
  end

  def validate
    super
    #validates_unique [:name]
  end
end
