# -*- coding: utf-8 -*-
# 仪表管理
class Instrument < Sequel::Model(:instruments)
  plugin :validation_helpers
  plugin :active_model
  many_to_one :vendor,class_name: "Vendor" # 厂商
  # many_to_one :category_range,class_name: "CategoryRange" # 检测范围
  many_to_one :instrument_kind,class_name: "InstrumentKind" # 仪器类型
  # many_to_one :category_precision,class_name: "CategoryPrecision" # 检测精度
  many_to_one :category,class_name: "Category" # 检测分类
  one_to_many :verifications,class_name: "Verification" # 检定申请纪录
  many_to_one :parent, class_name: "Instrument", key: "parent_id"
  many_to_one :organ, class_name: "Organ"
  many_to_one :regulation, class_name: "Regulation"
  one_to_many :notices,class_name: "Notice"


  def validate
    super
    # validates_unique [:sn], message: '已经存在'
    validates_presence [:sn, :instrument_kind_id, :specification,
      :date, :vendor_id, :category_id, :regulation_id]
  end


  def range_start
    /^([0-9\.]*)-([0-9\.]*)/.match(self.range)[1]
  end

  def range_end
    /^([0-9\.]*)-([0-9\.]*)/.match(self.range)[2]
  end

  # def detection_range
  #   self.try(:category_range).try(:range)
  # end

  def detection_name
    self.try(:instrument_kind).try(:name)
    #self.try(:category_type).try(:name)
  end

  # def detection_precision
  #   unless self.category_precision.blank?
  #     "#{self.try(:category_precision).try(:name)}级"
  #   end
  # end

  def self.without_report
    permit_calculation = %w(数字压力计 弹性元件式一般压力表)
    self.exclude(instrument_kind_id: InstrumentKind.where(name: permit_calculation).select_map(:id))
  end

  def vendor_name
    self.vendor.name
  end
end
