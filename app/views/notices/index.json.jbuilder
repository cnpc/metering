json.array!(@notices) do |notice|
  json.extract! notice, :id, :sn, :organ_id, :instrument_id, :conclousion, :approver, :verifier, :inspector, :verification_date, :expiry_date, :content
  json.url notice_url(notice, format: :json)
end
