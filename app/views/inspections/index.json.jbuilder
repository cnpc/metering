json.array!(@inspections) do |inspection|
  json.extract! inspection, :id, :title, :content, :attachment
  json.url inspection_url(inspection, format: :json)
end
