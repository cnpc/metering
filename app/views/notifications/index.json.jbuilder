json.array!(@notifications) do |notification|
  json.extract! notification, :id, :organ_id, :user_id, :notice_id, :checked, :content
  json.url notification_url(notification, format: :json)
end
