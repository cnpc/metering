class Paginate
	attr_accessor :objects
	attr_accessor :per
	attr_accessor :current_page
	attr_accessor :count
  def initialize objects,per,page
  	self.objects = objects
  	self.per = per
  	self.current_page = (page.nil? ? 1 : page.to_i)
  	self.count = objects.count
  end

  def start
  	objects.limit(per,(current_page - 1)*per)
  end

  def pages
  	build_pages(count,per,[]).size
  end

  private
  def build_pages total_count,default_count,result
  	if total_count > default_count
  		result << 1
  		build_pages((total_count-default_count),default_count,result)
  	else
  		result << 1
  	end
  	result
  end
end