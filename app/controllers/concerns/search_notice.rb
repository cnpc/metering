module SearchNotice
  def self.start arg
    rs = Instrument
    rs = rs.where(organ_id: arg["organ_id"]) unless arg["organ_id"].to_s.empty?
    rs = rs.where(instrument_kind_id: arg["instrument_kind_id"]) unless arg["instrument_kind_id"].to_s.empty?
    rs = rs.where(specification: arg["spec"]) unless arg["spec"].to_s.empty?
    rs = rs.where(sn: arg["sn"]) unless arg["sn"].to_s.empty?
    rs = rs.where(vendor_id: arg["vendor_id"]) unless arg["vendor_id"].to_s.empty?
    rs = rs.where{date <= arg["date_before"]} unless arg["date_before"].to_s.empty?
    rs = rs.where{date >= arg["date_after"]} unless arg["date_after"].to_s.empty?

    rs.all.map do |x|
        notice = notice_info(x)
        x.values.merge({
            kind:   x.instrument_kind.name,
            vendor: x.vendor.name,
            organ:  x.organ.name,
            notice: notice
        })
    end
  end


  private
  def self.notice_info dev
    notice = dev.notices.try(:last)
    {sn: notice.try(:verification).try(:sn), id: notice.try(:id)}
  end
end
