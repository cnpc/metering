# encoding: UTF-8
module ImportFile
  def self.import file,user_id
    path = File.join("public/imports", file.original_filename)
	  File.open(path, "wb") { |f| f.write(file.read) }
	  xlsx = Roo::Spreadsheet.open(path)
	  data = build_data_except_head xlsx
    messages = {count: data.size,errors: []}
	  data.each_with_index do |d,n|
      errors = save_data(d,user_id)
      if errors.size > 0
        result = {num: n+1,errors: []}
        result[:errors] = errors
        messages[:errors] << result
      end

	  end
    messages
  end

  def self.build_data_except_head xlsx
  	data = []
	  xlsx.each do |x|
	    data << x
	  end
	  data = data[1..data.size]
  end

  def self.save_data data,user_id
    errors = []
    if data[0].to_s.empty?
      errors << "仪器编号不能为空"
    end
    instrument = Instrument[sn: data[0].to_s]
    organ_id = (Organ[name: data[9]].id rescue nil)
    if instrument.blank?
      # validate
      if organ_id.nil?
        errors << "没有找到送检单位"
      end
      if data[2].to_s.empty?
        errors << "规格型号不能为空"
      end

      date = (Time.parse(data[3].to_s) rescue nil)
      if date.nil?
        errors << "生产日期有误"
      end
      vendor_id = (Vendor[name: data[4]].id rescue nil)
      if vendor_id.nil?
        errors << "没有找到生产厂商"
      end
      category_id = (Category[name: data[5]].id rescue nil)
      if category_id.nil?
        errors << "没有找到检定类别"
      end
      if data[6].to_s.empty?
        errors << "准确度等级不能为空"
      end
      if data[7].to_s.empty?
        errors << "测量范围不能为空"
      end
      regulation_id = (Regulation[name: data[8]].id rescue nil)
      if regulation_id.nil?
        errors << "没有找到检定依据"
      end
      instrument_kind_id = (InstrumentKind[name: data[1]].id rescue nil)
      if instrument_kind_id.nil?
        errors << "没有找到量器种类"
      end

      #---
      if errors.size.eql?(0)
        instrument = Instrument.create(sn: data[0],
          specification: data[2],precision: data[6],
          date: data[3],vendor_id: vendor_id,
          range: data[7],regulation_id: regulation_id,
          organ_id: organ_id,
          category_id: category_id,
          instrument_kind_id: instrument_kind_id)
      end
    end

    date = (Time.parse(data[10].to_s) rescue nil)
    if date.nil?
      errors << "送检日期有误"
    end
    if errors.size.eql?(0)
      Verification.create(date: data[10],
        organ_id: organ_id,instrument_id: instrument.id,user_id: user_id)
    end
    errors
  end
end