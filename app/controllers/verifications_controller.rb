# encoding: UTF-8
# 检测检定
class VerificationsController < ApplicationController
  load_and_authorize_resource class: "Verification"
  before_filter :check_file, only: [:process_import]

  def index
    @page_title = "检测检定管理"
    @verifications = Verification.order(:organ_id, :instrument_id).all
  end

  def organ
    @page_title = "检测检定管理"
    @verifications = Verification.order(:organ_id, :instrument_id).where(organ_id: current_user.organ_id)
  end

  def report
    @verification = Verification[id: params[:id]]
    @instruments = Instrument.where(standard: 1).all
    @data = @verification.report.data.blank? ? Report.default_data(@verification) : JSON.parse(@verification.report.data)
    render "reports/#{Settings.reports.send(@verification.instrument.instrument_kind.name)}"
  end

  def certificate
    # TODO: Read the certificate data from verification's notice
    @verification = Verification[id: params[:id]]
    @standard = @verification.standard
    @content = JSON.parse(@verification.notice.content)
    result_type = @content['qualified'] ? "证书" : "通知书"
    filename = "#{@verification.date}-#{@verification.organ.name}-#{@verification.instrument.detection_name}-#{result_type}"
    respond_to do |format|
      format.pdf do
        render pdf: filename,             # file name
               #template: 'layouts/application.pdf.slim',
               #layout: 'layouts/application.pdf.slim',
               #show_as_html: true
               layout: 'layouts/application.pdf.slim'
      end
    end
  end

  def calculate
    verification = Verification[params[:id]]
    verification.update(sn: params[:report]['纪录编号']) if params[:report]['纪录编号']
    @record = Report.verify verification, params[:report]
    is_certificate = @record['result']['pass'] ? 1 : 0
    verification.examined! if !verification.examined?

    if params[:report]['检定日期']
      verification.report.update(verification_date: params[:report]['检定日期'])
    end
    if params[:report]['核验日期']
      verification.report.update(checked_date: params[:report]['核验日期'])
    end
    redirect_to report_verification_path(params[:id])
  end

  def create
    if params[:instrument][:sn].present?
      @instrument = Instrument[sn: params[:instrument][:sn]]
      if @instrument.blank?
        @instrument = Instrument.create(instrument_params)
      end
      @verification = Verification.new(params[:instrument][:verification].merge(instrument_id: @instrument.id, user_id: current_user.id))
      if @verification.valid? and @verification.save
        redirect_to report_verification_path(@verification), success: '申请成功.'
      else
        flash[:alert]='请填写所有输入框数据'
        render :action=>:new
      end
    else
      flash[:alert]='请填写所有输入框数据'
      render :action=>:new
    end
  end

  def update
    if @verification.update(params[:verification])
      redirect_to :action => :index
    else
      render :action => :edit
    end
  end

  def update_state
    verification = Verification[params[:id]]
    verification.send("#{params[:state]}!")
    if params[:state].inquiry.approved?
      verification.generate_notice
    end

    if params[:state].inquiry.examined?
      redirect_to report_verification_path(verification)
    else
      redirect_to root_path
    end
  end
  #
  # def show
  # end

  def destroy
    @verification.destroy
    redirect_to :action => :index
  end

  def process_import
    @messages = ImportFile.import(@file,current_user.id)
    render "process_import"
  end

  private
  def check_file
    @file = params[:import_file]
    unless (@file.original_filename.split(".")[1]).to_s.eql?("xlsx")
      redirect_to action: :imports
    end
  end

  def instrument_params
    params.require(:instrument).permit(:sn,:organ_id,:instrument_kind_id,:specification,:date,
                                      :vendor_id,:category_id,:category_precision_id,
                                      :category_range_id,:basis, :regulation_id)
  end
end
