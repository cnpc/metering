# -*- coding: utf-8 -*-
class DashboardController < ApplicationController
  def index
  	@page_title ="待办事项"
    if current_user.common?
      @registered_verifications =
        Verification.registered.entries
      @examined_verifications   =
        Verification.examined.where("user_id != ?", current_user.id).entries
    else
      @verified_verifications   = Verification.verified.entries
    end
  end
end
