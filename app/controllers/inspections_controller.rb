class InspectionsController < ApplicationController
  load_and_authorize_resource
  skip_before_filter :verify_authenticity_token, :only => [:destroy]

  def index
    @page_title = "计量巡检信息"
    @inspections = Inspection.order(:date).reverse.all
  end

  def new
    @inspection = Inspection.new
  end

  def edit
  end

  def create
    @inspection = Inspection.new(inspection_params)
    if @inspection.valid?
      @inspection.save
      redirect_to inspections_path
    else
      render :new
    end
  end

  def update
    @inspection.update(inspection_params)
    if @inspection.valid?
      @inspection.save
      redirect_to inspections_path
    else
      render :edit
    end
  end

  def destroy
    render json: @inspection.destroy
  end

  private
    def inspection_params
      params.require(:inspection).permit(:title, :category, :attachment, :date)
    end
end
