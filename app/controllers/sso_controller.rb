# -*- coding: UTF-8 -*-

# 检定过程
class SsoController < ActionController::Base

    before_filter :fix_cas_session
    before_filter :ensure_loggedin

    def fix_cas_session
      if session[:cas].respond_to?(:with_indifferent_access)
        session[:cas] = session[:cas].with_indifferent_access
      end
    end

    def ensure_loggedin
      if session[:cas].nil? || session[:cas][:user].nil?
        render status: 401, text: "Redirecting to SSO..."
      end
    end


    helper_method :current_user

    def current_user
      return @current_user if @current_user
      if cas = request.session['cas']
        @current_user = CurrentUser.new
        @current_user.username = cas['user']
        user = User[username: @current_user.username]
        if user.nil?
          user = User.create(
              username: @current_user.username,
              email: "#{@current_user.username}@cnpc.com.cn")
        end
        @current_user.id = user.id
        @current_user.abilities = cas['extra_attributes']


        if User.count == 1
          user.role_id = Role.admin.id
          user.state = "approved"
          user.save
        end

        return @current_user
      end
    end


    helper_method :login_url
    helper_method :logout_url

    # def current_user
    #   return @current_user if @current_user
    #   if cas = request.session['cas']
    #     @current_user = CurrentUser.new
    #     @current_user.username = cas['user']
    #     @current_user.id = User.find_or_create(
    #         username: @current_user.username,
    #         email: "#{@current_user.username}@cnpc.com.cn").id
    #     @current_user.abilities = cas['extra_attributes']
    #     return @current_user
    #   end
    # end


    def login_url
      uri = URI(Rails.application.config.rack_cas.server_url)
      uri.path = '/login'
      uri.query = "service=#{request.url}"
      uri.to_s
    end

    def logout_url service_url=request.url
      uri = URI(root_url)
      uri.path = "/logout"
      uri.query = "service=#{service_url}&gateway=true"
      uri.to_s
    end
end
