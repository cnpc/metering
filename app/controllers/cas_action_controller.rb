# -*- coding: UTF-8 -*-

class CasActionController < SsoController
  before_filter :user_register

  # ------- 用户是否注册 --------
  def user_register
    user = User[current_user.id]
    render 'users/register.html', layout: 'blankframe' if user.unknown?
    render 'users/waiting.html',  layout: 'blankframe' if user.unauthorized?
  end
end
