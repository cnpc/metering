class NotificationsController < ApplicationController
  load_and_authorize_resource

  respond_to :html

  def index
    @page_title = "预警通知"
    @notifications = current_user.notifications_dataset.order(:created_at).reverse
    respond_with(@notifications)
  end

  def show
    @page_title = "预警通知"
    @notification.update(checked: true)
    respond_with(@notification)
  end

  private
    def notification_params
      params.require(:notification).permit(:organ_id, :user_id, :notice_id, :checked, :content, :created_at)
    end
end
