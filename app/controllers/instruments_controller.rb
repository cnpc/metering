# -*- coding: UTF-8 -*-
# 仪表管理
class InstrumentsController < ApplicationController
  load_and_authorize_resource class: "Instrument"

  def index
    @page_title = "计量器具管理"
    @instruments = Instrument.eager(:category)   # .where(organ_id: current_user.organ_id)
  end

  def search
    @instruments = Instrument.eager_graph(:organ, :regulation).where("instruments.sn LIKE ?", "%#{params[:query]}%")
    render json: @instruments
  end

  def show
    unless params[:id].blank?
      instrument = Instrument[id: params[:id]]
      render json: instrument.to_hash.merge(instrument_kind: instrument.instrument_kind.try(:name),
                                            vendor: instrument.vendor.try(:name),
                                            range: instrument.try(:range),
                                            precision: instrument.try(:precision),
                                            regulation: instrument.regulation.try(:sn))
    end
  end

  def expires

  end

  def create
    @instrument =  Instrument.new(params[:instrument])
    if @instrument.valid? and @instrument.save
      redirect_to :action =>:index
    else
      flash[:alert]='请填写所有输入框数据'
      render :action=>:new
    end
  end

  def update
    if @instrument.update(params[:instrument])
      redirect_to :action => :index
    else
      render :action => :edit
    end
  end

  def destroy
    @instrument.destroy
    redirect_to :action => :index
  end

end
