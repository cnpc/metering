class ApplicationController < CasActionController
  layout :layout_by_resource
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  #before_filter CASClient::Frameworks::Rails::Filter unless Rails.env.test?
  #before_filter :current_user
  #include SessionsHelper

  # before_filter :fix_cas_session
  # before_filter :ensure_loggedin

  # def fix_cas_session
  #   if session[:cas].respond_to?(:with_indifferent_access)
  #     session[:cas] = session[:cas].with_indifferent_access
  #   end
  # end

  # def ensure_loggedin
  #   if session[:cas].nil? || session[:cas][:user].nil?
  #     render status: 401, text: "Redirecting to SSO..."
  #   end
  # end


  # helper_method :current_user



  # before_action :configure_permitted_parameters, if: :devise_controller?
  # before_filter :authenticate_user!

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_url, :alert => exception.message
  end

  protected
  def layout_by_resource
    devise_controller? ? "login" : "application"
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:username, :email, :password, :password_confirmation, :remember_me) }
    devise_parameter_sanitizer.for(:sign_in) { |u| u.permit(:login, :username, :email, :password, :remember_me) }
    devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:username, :email, :password, :password_confirmation, :current_password) }
  end

end
