class SettingsController < ApplicationController
  load_and_authorize_resource class: "Setting"

  def index
    @settings = Setting.all
  end

  def create
    @setting =  Setting.new(params[:setting])
    if @setting.save
      redirect_to :action =>:index
    else
      render :action=>:new
    end
  end

  def update
    if @setting.update(params[:setting])
      redirect_to :action => :index
    else
      render :action => :edit
    end
  end

  def organ
    @page_title = "组织结构图"
    @setting = Setting[name: '组织结构图']
  end

  def destroy
    @setting.destroy
    redirect_to :action => :index
  end
end
