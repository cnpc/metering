# -*- coding: UTF-8 -*-
# 检测类型
class CategoryTypesController < ApplicationController
  load_and_authorize_resource class: "CategoryType"


  def index
    render json: CategoryType.all
  end


  def new
    @category_type = CategoryType.new(category_id: params[:category_id])
  end



  def create
    @category_type =  CategoryType.new(params[:category_type])
    @category_type[:category_id] = params[:category_id]
    if @category_type.save
      redirect_to category_url(params[:category_id])
    else
      render :action=>:new
    end
  end

  def update
    @category_type = CategoryType[params[:id]]
    if @category_type.update(params[:category_type])
      redirect_to category_url(params[:category_id])
    else
      render :action => :edit
    end
  end

  def destroy
    @category_type.destroy
    redirect_to category_url(params[:category_id])
  end


end
