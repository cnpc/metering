# -*- coding: UTF-8 -*-

# 预警(过期，报废)
class Api::EarlyWarningsController < SsoController
  def expires
    render json: DateValidate.expires(Instrument.all)
  end

  def scraps
  	render json: DateValidate.scraps(Instrument.all)
  end

end
