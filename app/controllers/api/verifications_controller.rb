# -*- coding: UTF-8 -*-

# 检定过程
class Api::VerificationsController < CasActionController

      # before_filter :fix_cas_session
      # before_filter :ensure_loggedin

      # def fix_cas_session
      #   if session[:cas].respond_to?(:with_indifferent_access)
      #     session[:cas] = session[:cas].with_indifferent_access
      #   end
      # end

      # def ensure_loggedin
      #   if session[:cas].nil? || session[:cas][:user].nil?
      #     render status: 401, text: "Redirecting to SSO..."
      #   end
      # end

      # helper_method :current_user

      # def current_user
      #   return @current_user if @current_user
      #   if cas = request.session['cas']
      #     @current_user = CurrentUser.new
      #     @current_user.username = cas['user']
      #     @current_user.id = User.find_or_create(
      #         username: @current_user.username,
      #         email: "#{@current_user.username}@cnpc.com.cn").id
      #     @current_user.abilities = cas['extra_attributes']
      #     return @current_user
      #   end
      # end




    def create
        sn = params[:data]["sn"]
        instrument = Instrument[sn: sn]

        req = params[:data].clone
        req.delete("verification")

        instrument = Instrument.create(req) if instrument.blank?
        verification = Verification.create(
            params[:data]["verification"].merge(instrument_id: instrument.id, user_id: current_user.id))

        render json: verification
    end
end
