# -*- coding: UTF-8 -*-

# 量器品种
class Api::InstrumentKindsController < ActionController::Base

    # 全部品类
    def index
        kinds = InstrumentKind.all.map {|e| e.values.merge({category: e.category.name})}
        render json: kinds
    end




    def show
        kind = InstrumentKind[params[:id]]
        kind[:vendors]    = kind.category.vendors
        kind[:precisions] = kind.accuracy.nil? ? [] : kind.accuracy.split(",")
        kind[:ranges]     = kind.regulation.nil? ? [] : kind.regulation.split(",")
        kind[:standards]  = Regulation.where(instrument_kind_id: params[:id]).all
        kind[:organs]     = Organ.all

        # category[:device_types] = category.category_types
        render json: kind
    end
end
