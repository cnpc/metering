# -*- coding: UTF-8 -*-

# 量器品种
class Api::OrgansController < SsoController

    # 全部单位
    def index
        organ = User[current_user.id].organ

        render json: [organ]   if organ.try(:scope) == 'SELF'
        render json: Organ.all
    end
end
