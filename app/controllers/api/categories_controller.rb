# -*- coding: UTF-8 -*-

# 检定类别
class Api::CategoriesController < ActionController::Base
    def index
        render json: Category.all
    end

    def show
        category = Category[params[:id]]
        category[:device_types] = category.instrument_kinds
        render json: category
    end
end
