# -*- coding: UTF-8 -*-

# 计量仪器

class Api::InstrumentsController < SsoController

    # --------- 创建计量仪器 ---------
    def create
        instrument = Instrument.new(params[:data])
        if instrument.valid? and instrument.save
            render json: instrument
        else
            render json: {result: :failure}
        end
    end


    # --------- 按品类与编号进行唯一性查找 ---------
    def unique
        kind_id = params[:kind_id]
        sn      = params[:sn]
        device  = Instrument.where(instrument_kind_id: kind_id, sn: sn).first

        if device.nil?
            return render json: {result: :failure, params: {instrument_kind_id: kind_id, sn: sn}}
        end

        render json: device
    end


    # --------- 查询复杂条件的仪器 ---------
    def search
        p organ = User[current_user.id].organ
        # query = organ.scope == 'ALL' ? params : params.merge(organ_id: organ.id)
        query = organ.nil? ? params : params.merge(organ_id: organ.id)
        render json: SearchNotice.start(query)
    end
end
