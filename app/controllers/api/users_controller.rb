# -*- coding: UTF-8 -*-

# 用户信息

class Api::UsersController < SsoController

    def index
        users = User.all.map do |x|
            x.values.merge(extra_attr(x))
        end
        render json: users
    end


    def create
        user = User[current_user.id]
        args = params["user"]

        user.update(realname: args["realname"],
                    organ_id: args["organ_id"],
                    role_id:  args["role_id"])
        user.register

        render json: {result: :success}
    rescue => ex
        render json: {result: :failure, reason: ex.to_s}
    end



    def approve
        user = User.where(username: params["id"]).first
        user.approve if user

        render json: user.values.merge(extra_attr(user))
    end


    private
    def extra_attr user
        {role: user.role.try(:i18n), organ: user.organ.try(:name), state_human: user.state_human}
    end
end
