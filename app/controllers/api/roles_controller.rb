# -*- coding: UTF-8 -*-

# 角色清单
class Api::RolesController < ActionController::Base

    def index
        render json: Role.all.map {|x| x.values.merge(i18n: x.i18n)}
    end
end
