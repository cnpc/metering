# -*- coding: UTF-8 -*-
# 检测分类
class CategoriesController < ApplicationController
  load_and_authorize_resource class: "Category"

  def index
    @page_title = "检测分类管理"

    respond_to do |format|
      format.html
      format.json { render json: @categories, layout: false }
    end
  end

  def show
    @category = Category[params[:id]]
    @page_title = @category.name + "分类"


  end



  def create
    @category =  Vendor.new(params[:category])
    if @category.save
      redirect_to :action =>:index
    else
      render :action=>:new
    end
  end

  def update

    if @category.update(params[:category])
      redirect_to :action => :index
    else
      render :action => :edit
    end
  end

  def destroy
    @category.destroy
    redirect_to :action => :index
  end


end
