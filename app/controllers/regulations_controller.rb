# -*- coding: UTF-8 -*-
# 生产厂商
class RegulationsController < ApplicationController
  load_and_authorize_resource class: "Regulation"
  skip_before_filter :verify_authenticity_token, :only => [:destroy]

  def index
    @page_title = "检定依据管理"
    @regulation_paginate = Paginate.new(Regulation,20,params[:page])
    @regulations = @regulation_paginate.start
  end

  def create
    @regulation =  Regulation.new(params[:regulation])
    if @regulation.valid? and @regulation.save
      redirect_to :action =>:index
    else
      render :action=>:new
    end
  end

  def update
    if @regulation.update(params[:regulation])
      redirect_to :action => :index
    else
      render :action => :edit
    end
  end

  def destroy
    render json: @regulation.destroy
  end
end
