class InstrumentKindsController < ApplicationController
	load_and_authorize_resource class: "InstrumentKind"
  skip_before_filter :verify_authenticity_token, :only => [:destroy]


  def index
    @instrument_kind_paginate = Paginate.new(InstrumentKind,20,params[:page])
    @instrument_kinds = @instrument_kind_paginate.start
  end

  def create
    @instrument_kind = InstrumentKind.new(params[:instrument_kind].merge(sn: Time.now.to_i))
    if @instrument_kind.valid? && @instrument_kind.save
      redirect_to action: :index
    else
    	render "new"
    end
  end

  def update
    if @instrument_kind.update(params[:instrument_kind])
      redirect_to :action => :index
    else
      render :action => :edit
    end
  end

  def destroy
    render json: @instrument_kind.destroy
  end
end