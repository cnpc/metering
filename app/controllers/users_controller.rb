class UsersController < ApplicationController
  skip_before_filter :require_no_authentication, :only => [ :new, :create]
  load_and_authorize_resource

  skip_before_filter :verify_authenticity_token, :only => [:destroy]

  def index
    @page_title = "系统用户管理"
    @user_paginate = Paginate.new(User,20,params[:page])
    @users = @user_paginate.start
  end

  def create
    @user = User.new(params[:user])
    if @user.valid?
      @user.save
      redirect_to action: :index
    else
      render action: :new
    end
  end

  def update
    user_params = params[:user].reject{ |k,v| v.empty? }
    if @user.update(user_params)
      redirect_to :action => :index
    else
      render :action => :edit
    end
  end

  def destroy
    render json: @user.destroy
  end
end
