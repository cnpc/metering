# -*- coding: UTF-8 -*-
# 检测范围
class CategoryRangesController < ApplicationController
  load_and_authorize_resource class: "CategoryRange"

  def new
    @category_range = CategoryRange.new(category_id: params[:category_id])
  end



  def create
    @category_range =  CategoryRange.new(params[:category_range])
    @category_range[:category_id] = params[:category_id]
    if @category_range.save
      redirect_to category_url(params[:category_id])
    else
      render :action=>:new
    end
  end

  def update
    @category_range = CategoryRange[params[:id]]
    if @category_range.update(params[:category_range])
      redirect_to category_url(params[:category_id])
    else
      render :action => :edit
    end
  end

  def destroy
    @category_range.destroy
    redirect_to category_url(params[:category_id])
  end


end
