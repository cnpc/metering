class DataLogsController < ApplicationController
  def index
    @page_title = "证书/通知书变更日志"
    @datalogs = DataLog.eager(:notice, :user).order(Sequel.desc(:created_at)).all
  end
end
