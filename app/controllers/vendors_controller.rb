# -*- coding: UTF-8 -*-
# 生产厂商
class VendorsController < ApplicationController
  load_and_authorize_resource class: "Vendor"
  skip_before_filter :verify_authenticity_token, :only => [:destroy]

  def index
    @page_title = "生产厂商管理"
  end

  def create
    @vendor =  Vendor.new(params[:vendor])
    if @vendor.valid? and @vendor.save
      redirect_to :action =>:index
    else
      render :action=>:new
    end
  end

  def update

    if @vendor.update(params[:vendor])
      redirect_to :action => :index
    else
      render :action => :edit
    end
  end

  def destroy
    render json: @vendor.destroy
  end


end
