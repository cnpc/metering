# -*- coding: utf-8 -*-
module Wdy
  module Unirole
    class UsersController < ApplicationController
      load_and_authorize_resource class: "Wdy::Unirole::User"
      def index 
        @page_title = "组织机构管理/用户管理"
      end
    end

  end
end
