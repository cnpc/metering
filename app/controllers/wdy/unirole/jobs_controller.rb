# -*- coding: utf-8 -*-
module Wdy
  module Unirole
    class JobsController < ApplicationController
      load_and_authorize_resource class: "Wdy::Unirole::Jobs"
      def index 
        @page_title = "组织机构管理/岗位管理"
      end

      def create
      	@job =  Wdy::Unirole::Jobs.new(params[:wdy_unirole_jobs])
    if @job.save
      redirect_to :action =>:index
    else
      render :action=>:new
    end
  end

  def update

    if @job.update(params[:wdy_unirole_jobs])
      redirect_to :action => :index
    else
      render :action => :edit
    end
  end

  def destroy
    @job.destroy
    redirect_to :action => :index
  end


    end

  end
end
