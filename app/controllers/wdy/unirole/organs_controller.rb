# -*- coding: utf-8 -*-
module Wdy
  module Unirole
    class OrgansController < ApplicationController
      load_and_authorize_resource class: "Wdy::Unirole::Organ"
      def index 
        @page_title = "组织机构管理/组织机构管理"
        @organs = Wdy::Unirole::Organ.order(Sequel.asc(:parentid)).all
      end

      def create
        @organ =  Wdy::Unirole::Organ.new(params[:wdy_unirole_organ])
    if @organ.save
      redirect_to :action =>:index
    else
      render :action=>:new
    end
  end

  def update

    if @organ.update(params[:wdy_unirole_organ])
      redirect_to :action => :index
    else
      render :action => :edit
    end
  end

  def destroy
    @organ.destroy
    redirect_to :action => :index
  end


    end

  end
end
