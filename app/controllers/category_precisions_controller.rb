# -*- coding: UTF-8 -*-
# 检测精度
class CategoryPrecisionsController < ApplicationController
  load_and_authorize_resource class: "CategoryPrecision"

  def new
    @category_precision = CategoryPrecision.new(category_id: params[:category_id])
  end



  def create
    @category_precision =  CategoryPrecision.new(params[:category_precision])
    @category_precision[:category_id] = params[:category_id]
    if @category_precision.save
      redirect_to category_url(params[:category_id])
    else
      render :action=>:new
    end
  end

  def update
    @category_precision = CategoryPrecision[params[:id]]
    if @category_precision.update(params[:category_precision])
      redirect_to category_url(params[:category_id])
    else
      render :action => :edit
    end
  end

  def destroy
    @category_precision.destroy
    redirect_to category_url(params[:category_id])
  end


end
