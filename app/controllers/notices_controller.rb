class NoticesController < ApplicationController
  load_and_authorize_resource class: "Notice"
  before_filter :default_format_pdf, only: :show
  before_action :set_parsed_content, only: [:edit_inside_page, :show]

  respond_to :html, :pdf

  def index
    # @notice = params[:notice].nil? ? {} : JSON.parse(params[:notice])
    # @page_title = "检定证书/通知书"
    # @notice_paginate = Paginate.new(SearchNotice.start(@notice),50,params[:page])
    # @notices = @notice_paginate.start
    # respond_with(@notices)
  end

  def edit_inside_page
    @page_title = @notice.instrument.detection_name
  end

  def show
    @verification = @notice.verification
    @standard = @verification.try(:standard)
    result_type = @notice.certificate ? "证书" : "通知书"
    filename = "#{@notice.verification_date}-#{@notice.organ.name}-#{@notice.instrument.detection_name}-#{result_type}"
    respond_to do |format|
      format.pdf do
        render pdf: filename
      end
    end
  end

  def create
    @notice = Notice.new(notice_params)
    @notice.save
    redirect_to edit_inside_page_notice_path(@notice)
  end

  def update
    @notice.update(notice_params)
    respond_with(@notice)
  end

  def update_content
    previous_content = JSON.parse(@notice.content)
    @notice.update(content: params[:notice][:content].to_json)

    latest_content = JSON.parse(@notice.content)
    diff = HashDiff.best_diff(previous_content, latest_content)
    DataLog.create(user_id: current_user.id, notice_id: @notice.id, content: diff.to_json)
    redirect_to notices_path
  end

  def destroy
    @notice.destroy
    respond_with(@notice)
  end

  def search
    @page_title = "检定证书/通知书"
    @notice = params[:notice]
    @notice_paginate = Paginate.new(SearchNotice.start(@notice),50,params[:page])
    @notices = @notice_paginate.start
    render "index"
  end

  private
    def notice_params
      params.require(:notice).permit(:sn, :organ_id, :instrument_id, :conclusion, :approver, :verifier, :inspector, :verification_date, :expiry_date, :content, :certificate)
    end
     def set_parsed_content
       @content = @notice.content.present? ? JSON.parse(@notice.content) : Notice.default_data(@notice)
       @report  = JSON.parse(@notice.verification.report.data)
     end

    def default_format_pdf
      request.format = "pdf" unless params[:format]
    end
end
