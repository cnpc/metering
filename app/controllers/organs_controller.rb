# -*- coding: utf-8 -*-
class OrgansController < ApplicationController
  load_and_authorize_resource class: "Organ"

  skip_before_filter :verify_authenticity_token, :only => [:destroy]

  def index
    @page_title = "送检单位管理"
    @organ_paginate = Paginate.new(Organ,20,params[:page])
    @organs = @organ_paginate.start
  end

  def create
    @organ =  Organ.new(params[:organ])
    if @organ.save
      redirect_to :action =>:index
    else
      render :action=>:new
    end
  end

  def update
    if @organ.update(params[:organ])
      redirect_to :action => :index
    else
      render :action => :edit
    end
  end

  def destroy
    render json: @organ.destroy
  end
end

