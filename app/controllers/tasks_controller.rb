# -*- coding: UTF-8 -*-
class TasksController < ApplicationController
  load_and_authorize_resource class: "Task"
  def index
    render json: Task.where(who: User.roles(current_user), state: 'todo')
  end


  def show
    task = Task[params[:id]]
    render json: task.values.merge(params: JSON.parse(task.params))
  end

  def edit
    @task = Task[params[:id]].values
    @task[:req_over] = nil
    @task
  end

  def update
    task = Task[params[:id]]
    workflow = eng[task.sid]
    params["data"] ={req_over: "required"}
    @data = params[:data]
    
    # 测试用值
    @data = {data: [req_over: "required"]}
    
    workflow['data'] = workflow['data'] || {}
    @data.each {|k,v| workflow['data'][k] = v} if @data
    workflow['data']['current_user'] = current_user

    eng.proceed(workflow)
    render json: {result: 'ok'}
  end


  private
  def eng
    Workflow.engine.storage_participant
  end

end
