{
  "检定员" => nil,
  "核验员" => nil,
  "检定证书号" => nil,
  "检定日期" => nil,
  "主标准器名称" => nil,
  "标准器器号" => nil,
  "主标准器生产厂商" => nil,
  "开孔圆筒直径1" => nil,
  "开孔圆筒直径2" => nil,
  "开孔圆筒直径3" => nil,
  "开孔圆筒直径4" => nil,
  "开孔圆筒直径平均值" => nil,
  "开孔圆筒直径平均值差" => nil,
  "厚度E0" => nil,
  "厚度E90" => nil,
  "厚度E180" => nil,
  "厚度E270" => nil,
  "厚度E差值" => nil,
  "厚度E平均值" => nil,
  "厚度e0" => nil,
  "厚度e90" => nil,
  "厚度e180" => nil,
  "厚度e270" => nil,
  "厚度e差值" => nil,
  "厚度e平均值" => nil,
  "平面度" => nil,
  "平面度结论" => nil,
  "表面粗糙度AE" => nil,
  "表面粗糙度B" => nil,
  "直角入口" => nil,
  "圆筒出口" => nil,
  "圆锥出口" => nil,
  "斜角" => nil,
  "粗糙度结论" => nil,
  "原孔板孔径值" => nil,
  "环境温度" => nil,
  "外观" => nil,
  "标准器" => {
    "0" => nil,
    "1" => nil
  },

  "检定结果" => {
    "检定点" => [nil] * 10,
    "实际输出值" => {
      "第一次" => {
        "上行程" => [nil] * 10,
        "下行程" => [nil] * 10
      },
      "第二次" => {
        "上行程" => [nil] * 10,
        "下行程" => [nil] * 10
      },
      "第三次" => {
        "上行程" => [nil] * 10,
        "下行程" => [nil] * 10
      }
    },

    "理论输出值" => [nil] * 10
  },

  "计算结果" => {
    "环境温度" => nil,
    "环境湿度" => nil,
    "绝缘电阻" => nil,
    "绝缘强度" => nil,
    "密封性" => nil,
    "外观检查" => nil,
    "允许误差" => nil,
    "最大误差" => nil,
    "允许回差" => nil,
    "最大回差" => nil,
    "基本误差" => [nil] * 10,
    "回差" => [nil] * 10
  }
}
