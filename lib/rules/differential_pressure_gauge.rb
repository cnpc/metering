engine = Wongi::Engine.create

engine << ["双波纹差压", "允许示值误差", @facts['差压']["允许示值误差"].to_f.abs]
engine << ["双波纹差压", "最大示值误差", @facts['差压']["最大示值误差"].to_f.abs]
engine << ["双波纹差压", "允许回程误差", @facts['差压']["允许回程误差"].to_f.abs]
engine << ["双波纹差压", "最大回程误差",    @facts['差压']["最大回程误差"].to_f.abs]
engine << ["双波纹差压", "允许重复性误差",   @facts['差压']["允许重复性误差"].to_f.abs]
engine << ["双波纹差压", "最大重复性误差",   @facts['差压']["最大重复性误差"].to_f.abs]
engine << ["双波纹差压", "耐压强度和密封性", @facts['差压']["耐压强度和密封性"].to_f.abs]
engine << ["双波纹差压", "外观检查",        @facts['差压']["外观检查"].to_s]


engine << ["双波纹静压", "允许示值误差", @facts['静压']["允许示值误差"].to_f.abs]
engine << ["双波纹静压", "最大示值误差", @facts['静压']["最大示值误差"].to_f.abs]
engine << ["双波纹静压", "允许回程误差", @facts['静压']["允许回程误差"].to_f.abs]
engine << ["双波纹静压", "最大回程误差", @facts['静压']["最大回程误差"].to_f.abs]
engine << ["双波纹静压", "允许重复性误差", @facts['静压']["允许重复性误差"].to_f.abs]
engine << ["双波纹静压", "最大重复性误差", @facts['静压']["最大重复性误差"].to_f.abs]
engine << ["双波纹静压", "静压起点格", @facts['静压']["静压起点格"].to_f.abs]

rules = {

  "允许示值误差不能超过0.003Mpa" => (engine.rule "" do
    forall {
      has "双波纹静压", :Name , :Value
      same "允许示值误差", :Name
      greater :Value, 0.003
    }
  end),

  "最大示值误差不能超过0.003Mpa" => (engine.rule "" do
    forall {
      has "双波纹静压", :Name, :Value
      same :Name, "最大示值误差"
      greater :Value, 0.003
    }
  end),


  "静压起点格不能超过0.003Mpa" => (engine.rule "" do
    forall {
      has "双波纹静压", :Name, :Value
      same :Name, "静压起点格"
      greater :Value, 0.003
    }
  end),


  "外观检查必须合格" => (engine.rule "" do
    forall {
      has "双波纹差压", :Name, :Value
      same :Name, "外观检查"
      diff :Value, "合格"
    }
  end),


  "静压起点格不能低于20M欧"    => (engine.rule "" do
    forall {
      has "双波纹静压", :Name, :Value
      same :Name, "静压起点格"
      less :Value, 20
    }
  end),

}

result = rules.inject({}) do |r, (k,v)|
  v.tokens.each do |token|
    r[k] = (r[k] || {}).merge({token[:Name] => token[:Value]})
  end
  r
end

{pass: result.empty?, message: result}
