engine = Wongi::Engine.create

engine << ["被检器", "外观检查", @facts["外观检查"].to_s]
engine << ["被检器", "零位误差", @facts["零位误差"].to_s]
engine << ["被检器", "指针偏转平稳性", @facts["指针偏转平稳性"].to_s]
engine << ["被检器", "最大示值误差", @facts["最大示值误差"]]
engine << ["被检器", "最大回程误差", @facts["最大回程误差"]]
engine << ["被检器", "最大允许误差", @facts["最大允许误差"]]
engine << ["被检器", "其他", @facts["其他"]]

rules = {
  "外观检查必须合格" => (engine.rule "" do
    forall {
      has "被检器", :Name, :Value
      same :Name, "外观检查"
      diff :Value, "合格"
    }
  end),

  "零位误差必须合格" => (engine.rule "" do
    forall {
      has "被检器", :Name, :Value
      same :Name, "零位误差"
      diff :Value, "合格"
    }
  end),


  "指针偏转平稳性必须合格" => (engine.rule "" do
    forall {
      has "被检器", :Name, :Value
      same :Name, "指针偏转平稳性"
      diff :Value, "合格"
    }
  end),

  "示值误差不能超过#{@facts['最大允许误差'].abs}" => (engine.rule "" do
    forall {
      has "被检器", :Allow, :Allow_value
      has "被检器", :Name, :Value
      same :Allow, "最大允许误差"
      same :Name, "最大示值误差"
      greater :Value, :Allow_value
    }
  end),

  "回程误差不大于#{@facts['最大允许误差'].abs}" => (engine.rule "" do
    forall {
      has "被检器", :Allow, :Allow_value
      has "被检器", :Name, :Value
      same :Allow, "最大允许误差"
      same :Name, "最大回程误差"
      less :Allow_value,:Value
    }
  end),

  "其他必须合格" => (engine.rule "" do
    forall {
      has "被检器", :Name, :Value
      same :Name, "其他"
      diff :Value, "合格"
    }
  end)
}

result = rules.inject({}) do |r, (k,v)|
  v.tokens.each do |token|
    r[k] = (r[k] || {}).merge({token[:Name] => token[:Value]})
  end
  r
end

{pass: result.empty?, message: result}
