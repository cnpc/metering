engine = Wongi::Engine.create

engine << ["被检器", "示值允许值", @facts["示值最大值与最小值之"]["允许值"].to_f]
engine << ["被检器", "示值差值", @facts["示值最大值与最小值之"]["差值"].to_f]
engine << ["被检器", "指针最大示值允许值", @facts["指针最大示值"]["允许值"].to_f]
engine << ["被检器", "指针最大示值变动量", @facts["指针最大示值"]["变动量"].to_f]
engine << ["被检器", "检定点允许值", @facts["检定点各次的读数与该点标称值与该点标称值的"]["允许值"].to_f]
engine << ["被检器", "检定点最大偏差", @facts["检定点各次的读数与该点标称值与该点标称值的"]["最大偏差"].to_f]

rules = {

  "示值差值必须小于#{@facts['示值最大值与最小值之']['允许值']}" => (engine.rule "" do
    forall {
      has "被检器", :Allow_name, :Allow_value
      has "被检器", :Name, :Value
      same :Allow_name, "示值允许值"
      same :Name, "示值差值"
      greater :Value, :Allow_value
    }
  end),

  "指针最大示值变动量必须小于#{@facts['指针最大示值']['允许值']}" => (engine.rule "" do
    forall {
      has "被检器", :Allow_name, :Allow_value
      has "被检器", :Name, :Value
      same :Allow_name, "指针最大示值允许值"
      same :Name, "指针最大示值变动量"
      greater :Value, :Allow_value
    }
  end),

  "检定点最大偏差必须小于#{@facts['检定点各次的读数与该点标称值与该点标称值的']['允许值']}" => (engine.rule "" do
    forall {
      has "被检器", :Allow_name, :Allow_value
      has "被检器", :Name, :Value
      same :Allow_name, "检定点允许值"
      same :Name, "检定点最大偏差"
      greater :Value, :Allow_value
    }
  end),
}

result = rules.inject({}) do |r, (k,v)|
  v.tokens.each do |token|
    r[k] = (r[k] || {}).merge({token[:Name] => token[:Value]})
  end
  r
end

{pass: result.empty?, message: result}
