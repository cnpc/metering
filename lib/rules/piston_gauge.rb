engine = Wongi::Engine.create

engine << ["被检器", "密封性", @facts["密封性"].to_s]
engine << ["被检器", "外观", @facts["外观"].to_s]

rules = {

  "密封性必须合格" => (engine.rule "" do 
    forall {
      has "被检器", :Name, :Value
      same :Name, "密封性"
      diff :Value, "合格"
    }
  end),

  "外观必须合格" => (engine.rule "" do
    forall {
      has "被检器", :Name, :Value
      same :Name, "外观"
      diff :Value, "合格"
    }
  end)
  
}

result = rules.inject({}) do |r, (k,v)|
  v.tokens.each do |token|
    r[k] = (r[k] || {}).merge({token[:Name] => token[:Value]})
  end
  r
end

if result.empty?
  conclusion = "合格"
else
  conclusion = "不合格"
end

{pass: result.empty?, message: result, conclusion: conclusion}
