engine = Wongi::Engine.create

engine << ["被检器", "绝缘电阻", @facts["绝缘电阻"].to_s]
engine << ["被检器", "绝缘强度", @facts["绝缘强度"].to_s]
engine << ["被检器", "密封性", @facts["密封性"].to_s]
engine << ["被检器", "外观检查", @facts["外观检查"].to_s]
engine << ["被检器", "允许误差", @facts["允许误差"].to_f.abs]
engine << ["被检器", "回程误差", @facts["最大回差"].to_f]
engine << ["被检器", "示值误差", @facts["最大误差"].to_f]

rules = {
  
  "绝缘电阻必须合格" => (engine.rule "" do 
    forall {
      has "被检器", :Name, :Value
      same :Name, "绝缘电阻"
      diff :Value, "合格"
    }
  end),

  "绝缘强度必须合格" => (engine.rule "" do 
    forall {
      has "被检器", :Name, :Value
      same :Name, "绝缘强度"
      diff :Value, "合格"
    }
  end),

  "密封性必须合格" => (engine.rule "" do 
    forall {
      has "被检器", :Name, :Value
      same :Name, "密封性"
      diff :Value, "合格"
    }
  end),

  "外观检查必须合格" => (engine.rule "" do
    forall {
      has "被检器", :Name, :Value
      same :Name, "外观检查"
      diff :Value, "合格"
    }
  end),
  
  "回程误差不能超过#{@facts['允许误差'].abs}" => (engine.rule "" do
    forall {
      has "被检器", :Allow, :Allow_value
      has "被检器", :Name, :Value
      same :Allow, "允许误差"
      same :Name, "回程误差"
      greater :Value, :Allow_value
    }
  end),

  "示值误差不能超过0.003Mpa"    => (engine.rule "" do
    forall {
      has "被检器", :Name, :Value
      same :Name, "示值误差"
      greater :Value, 0.003
    }
  end),
}

result = rules.inject({}) do |r, (k,v)|
  v.tokens.each do |token|
    r[k] = (r[k] || {}).merge({token[:Name] => token[:Value]})
  end
  r
end

{pass: result.empty?, message: result}
