engine = Wongi::Engine.create

engine << ["被检器", "示值误差最大偏移", @facts["示值误差"].to_f.abs]
engine << ["被检器", "回程误差最大偏移", @facts["回程误差"].to_f.abs]
engine << ["被检器", "零位漂移最大值", @facts["零位漂移"].to_f.abs]
engine << ["被检器", "外观检查", @facts["外观检查"].to_s]
engine << ["被检器", "绝缘电阻", @facts["绝缘电阻"].to_f]

rules = {

  "零位漂移不能超过0.003Mpa" => (engine.rule "" do
    forall {
      has "被检器", :Name , :Value
      same "零位漂移最大值", :Name
      greater :Value, 0.003
    }
  end),

  "示值误差不能超过0.003Mpa"    => (engine.rule "" do
    forall {
      has "被检器", :Name, :Value
      same :Name, "示值误差最大偏移"
      greater :Value, 0.003
    }
  end),


  "回程误差不能超过0.003Mpa"    => (engine.rule "" do
    forall {
      has "被检器", :Name, :Value
      same :Name, "回程误差最大偏移"
      greater :Value, 0.003
    }
  end),


  "外观检查必须合格"    => (engine.rule "" do
    forall {
      has "被检器", :Name, :Value
      same :Name, "外观检查"
      diff :Value, "合格"
    }
  end),


  "绝缘电阻不能低于20M欧"    => (engine.rule "" do
    forall {
      has "被检器", :Name, :Value
      same :Name, "绝缘电阻"
      less :Value, 20
    }
  end),

}

result = rules.inject({}) do |r, (k,v)|
  v.tokens.each do |token|
    r[k] = (r[k] || {}).merge({token[:Name] => token[:Value]})
  end
  r
end

{pass: result.empty?, message: result}
