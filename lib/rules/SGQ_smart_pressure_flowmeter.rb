engine = Wongi::Engine.create

engine << ["被检器", "外观检查", @facts["外观检查"].to_s]
engine << ["被检器", "允许误差", @facts["允许误差"].to_f.abs]
engine << ["被检器", "压力允许值", @facts["压力示值"]["允许值"].to_f.abs]
engine << ["被检器", "压力误差最大值", @facts["压力示值"]["实际最大值"].to_f.abs]
engine << ["被检器", "差压允许值", @facts["差压示值"]["允许值"].to_f.abs]
engine << ["被检器", "差压误差最大值", @facts["差压示值"]["实际最大值"].to_f.abs]
engine << ["被检器", "温度允许值", @facts["温度示值"]["允许值"].to_f.abs]
engine << ["被检器", "温度误差最大值", @facts["温度示值"]["实际最大值"].to_f.abs]

rules = {
  
  "外观检查必须合格" => (engine.rule "" do
    forall {
      has "被检器", :Name, :Value
      same :Name, "外观检查"
      diff :Value, "合格"
    }
  end),
  
  "压力误差不能超过#{@facts['压力示值']['允许值'].abs}" => (engine.rule "" do
    forall {
      has "被检器", :Allow, :Allow_value
      has "被检器", :Name, :Value
      same :Allow, "压力允许值"
      same :Name, "压力误差最大值"
      greater :Value, :Allow_value
    }
  end),

  "差压误差不能超过#{@facts['差压示值']['允许值'].abs}" => (engine.rule "" do
    forall {
      has "被检器", :Allow, :Allow_value
      has "被检器", :Name, :Value
      same :Allow, "差压允许值"
      same :Name, "差压误差最大值"
      greater :Value, :Allow_value
    }
  end),

  "温度误差不能超过#{@facts['温度示值']['允许值'].abs}" => (engine.rule "" do
    forall {
      has "被检器", :Allow, :Allow_value
      has "被检器", :Name, :Value
      same :Allow, "温度允许值"
      same :Name,"温度误差最大值"
      greater :Value, :Allow_value
    }
  end)
}

result = rules.inject({}) do |r, (k,v)|
  v.tokens.each do |token|
    r[k] = (r[k] || {}).merge({token[:Name] => token[:Value]})
  end
  r
end

{pass: result.empty?, message: result}
