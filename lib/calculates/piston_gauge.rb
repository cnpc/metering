@record["计算结果"]["活塞下降速度"]={}
@record["计算结果"]["活塞下降速度"]["最大"] = @record["活塞下降速度"].map(&:to_f).max

all_seconds =  0
@record["活塞转动延续时间"].values.each do |time|
  all_seconds += time["min"].to_i * 60 + time["s"].to_i
end
@record["计算结果"]["活塞转动延续时间"]={}
@record["计算结果"]["活塞转动延续时间"]["min"] = (all_seconds / 180).round
@record["计算结果"]["活塞转动延续时间"]["s"] = (all_seconds % 60 / 3).round

@record["活塞有效面积的检定"]["被检活塞式压力计第i个检定点相对于平衡点增加的砝码质量"].each_with_index do |e, index|
  if !@record["活塞有效面积的检定"]["标准活塞式压力计第i个检定点相对于平衡点增加的砝码质量"][index].blank?
    @record["计算结果"]["被检活塞有效面积单独值"][index] = @record["有效面积"].to_f * e.to_f / 
      @record["活塞有效面积的检定"]["标准活塞式压力计第i个检定点相对于平衡点增加的砝码质量"][index].to_f 
  end
end
check_times = @record['计算结果']['被检活塞有效面积单独值'].count{|v| !v.nil? }

@record["计算结果"]["活塞有效面积平积值"] =
  @record["计算结果"]["被检活塞有效面积单独值"].map{|v| v.to_f}.sum / check_times if check_times != 0

sumsq = @record["计算结果"]["被检活塞有效面积单独值"].collect{|e| (e.to_f - @record["计算结果"]["活塞有效面积平积值"]) ** 2 unless e.nil? }
@record["计算结果"]["SA"] = Math.sqrt(sumsq.map{|v| v.to_f}.sum / (check_times - 1)) if check_times > 1
@record["计算结果"]["A"] = 3 * @record["计算结果"]["SA"].to_f
@record
