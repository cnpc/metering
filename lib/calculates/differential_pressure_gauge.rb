repeat_value = []

@record["差压"].each do |e|
  repeat_value.push(e["重复性"]["正向"].to_f, e["重复性"]["反向"].to_f)
  e['示值误差']['正向'] = e['实际差压']['正向'].to_f - e['计算差压'].to_f
  e['示值误差']['反向'] = e['实际差压']['反向'].to_f - e['计算差压'].to_f
  e['回程误差'] = (e['示值误差']['正向'].to_f - e['示值误差']['反向'].to_f).abs

  max_value = [e['示值误差']['正向'].to_f, e['示值误差']['反向'].to_f].max
  @record["计算结果"]["差压"]["最大示值误差"] = max_value if max_value > @record['计算结果']['差压']['最大示值误差'].to_f
@record["计算结果"]["差压"]["最大回程误差"] = e['回程误差'] if e['回程误差'] > @record["计算结果"]["差压"]["最大回程误差"].to_f
end
@record["计算结果"]["差压"]["允许示值误差"] = ((@record["差压测量范围上限"].to_f - @record["差压测量范围下限"].to_f) *
  @record["准确度等级"].to_f)/100.abs
@record["计算结果"]["差压"]['允许回程误差'] = @record["计算结果"]["差压"]["允许示值误差"].to_f * 0.8
@record["计算结果"]["差压"]["允许重复性误差"] = @record["精确等级"].to_f * 0.4
@record["计算结果"]["差压"]["最大重复性误差"] = (repeat_value.max - repeat_value.min) / 6 * 0.5  * 100


@record["静压"]["标准仪表示值"]["计算值"].each_with_index do |e, index|
  @record["静压"]["示值误差"]["正向"][index] = e.to_f - @record["静压"]["被检仪表值"]["正向"][index].to_f
  @record["静压"]["示值误差"]["反向"][index] = e.to_f - @record["静压"]["被检仪表值"]["反向"][index].to_f
  @record["静压"]["回程误差"][index] = (
    @record["静压"]["被检仪表值"]["正向"][index].to_f - @record["静压"]["被检仪表值"]["反向"][index].to_f
  ).abs
  max_value = [@record["静压"]['示值误差']['正向'][index].to_f, 
               @record["静压"]['示值误差']['反向'][index].to_f].max

  if max_value > @record["计算结果"]["静压"]["最大示值误差"].to_f
    @record["计算结果"]["静压"]["最大示值误差"] = max_value
  end
  if @record["静压"]['回程误差'][index] > @record["计算结果"]["差压"]["最大回程误差"].to_f
    @record["计算结果"]["静压"]["最大回程误差"] = @record["静压"]["回程误差"][index] 
  end
end
@record["计算结果"]["静压"]["允许示值误差"] = ((@record["静压测量范围上限"].to_f - @record["静压测量范围下限"].to_f) *
  @record["准确度等级"].to_f)/100.abs
@record["计算结果"]["静压"]['允许回程误差'] = @record["计算结果"]["静压"]["允许示值误差"].to_f * 0.8
@record["计算结果"]["静压"]["允许重复性误差"] = @record["精确等级"].to_f * 0.4

@record
