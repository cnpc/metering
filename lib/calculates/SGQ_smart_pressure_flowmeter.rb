# 压力计算
@record["压力示值"]["理论输出值"].each_with_index do |e, index|
  @record["计算结果"]["压力示值"]["示值误差"][index] = [
    @record["压力示值"]["显示值"]["上行"][index].to_f - e.to_f,
    @record["压力示值"]["显示值"]["下行"][index].to_f - e.to_f
  ].max
end
@record["计算结果"]["压力示值"]["允许值"] = ((
    @record["压力示值"]["测量范围上限"].to_f - @record["压力示值"]["测量范围下限"].to_f
  ) * @record["压力示值"]["准确度等级"].to_f)/100.abs
@record["计算结果"]["压力示值"]["实际最大值"] = @record["计算结果"]["压力示值"]["示值误差"].max

# 差压计算
@record["差压示值"]["理论输出值"].each_with_index do |e, index|
  @record["计算结果"]["差压示值"]["示值误差"][index] = [
    @record["差压示值"]["显示值"]["上行"][index].to_f - e.to_f,
    @record["差压示值"]["显示值"]["下行"][index].to_f - e.to_f
  ].max
end
@record["计算结果"]["差压示值"]["允许值"] = ((
    @record["差压示值"]["测量范围上限"].to_f - @record["差压示值"]["测量范围下限"].to_f
  ) * @record["差压示值"]["准确度等级"].to_f)/100.abs
@record["计算结果"]["差压示值"]["实际最大值"] = @record["计算结果"]["差压示值"]["示值误差"].max

# 温度计算
@record["温度示值"]["理论输出值"].each_with_index do |e, index|
  @record["计算结果"]["温度示值"]["示值误差"][index] = [
    @record["温度示值"]["显示值"]["上行"][index].to_f - e.to_f,
    @record["温度示值"]["显示值"]["下行"][index].to_f - e.to_f
  ].max
end
@record["计算结果"]["温度示值"]["允许值"] = ((
    @record["温度示值"]["测量范围上限"].to_f - @record["温度示值"]["测量范围下限"].to_f
  ) * @record["温度示值"]["准确度等级"].to_f)/100.abs
@record["计算结果"]["温度示值"]["实际最大值"] = @record["计算结果"]["温度示值"]["示值误差"].max

@record
