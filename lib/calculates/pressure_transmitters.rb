actual_value = @record["检定结果"]["实际输出值"]
@record["计算结果"]["基本误差"] = []
@record["计算结果"]["回差"] = []

@record["检定结果"]["检定点"].each_with_index do |e, index|
    @record["计算结果"]["基本误差"][index] = [
      actual_value["第一次"]["上行程"][index].to_f,
      actual_value["第一次"]["下行程"][index].to_f,
      actual_value["第二次"]["上行程"][index].to_f,
      actual_value["第二次"]["下行程"][index].to_f,
      actual_value["第三次"]["上行程"][index].to_f,
      actual_value["第三次"]["下行程"][index].to_f,
    ].max - @record["检定结果"]["理论输出值"][index].to_f

    @record["计算结果"]["回差"][index] = [
      actual_value["第一次"]["上行程"][index].to_f - actual_value["第一次"]["下行程"][index].to_f, 
      actual_value["第二次"]["上行程"][index].to_f - actual_value["第二次"]["下行程"][index].to_f, 
      actual_value["第三次"]["上行程"][index].to_f - actual_value["第三次"]["下行程"][index].to_f, 
    ].max
end

@record["计算结果"]["最大回差"] = @record["计算结果"]["回差"].max
@record["计算结果"]["最大误差"] = @record["计算结果"]["基本误差"].max
@record["计算结果"]["允许误差"] = ((@record["测量范围上限"].to_f - @record["测量范围下限"].to_f) *
  @record["准确度等级"].to_f)/100.abs
@record["计算结果"]["允许回差"] = @record["计算结果"]["允许误差"].abs
@record
