@record["示值检定"].each_value do |e|
  v = e["周期稳定性"]
  v["正行程示值误差"] = v["正行程示值"].to_f - e["标准器示值"].to_f
  v["反行程示值误差"] = v["反行程示值"].to_f - e["标准器示值"].to_f

  v = e["示值误差"].values
  e["示值误差最大值"] = (v.map {|s| [s["正行程"], s["反行程"]]}.flatten).map {|s| (s.to_f - e["标准器示值"].to_f).abs}.max
  e["回程误差最大值"] = (v.map {|s| s["正行程"].to_f - s["反行程"].to_f}.flatten).map(&:to_f).map(&:abs).max
  e["正行程平均值"] = (v.map {|s| s["正行程"].to_f }).sum / v.size
  e["反行程平均值"] = (v.map {|s| s["反行程"].to_f }).sum / v.size
end

@record["计算结果"]["零位漂移"] = @record["零位漂移"].values.map {|e| e["示值"].to_f.abs}.max
@record["计算结果"]["示值误差"] = @record["示值检定"].values.map {|e| e["示值误差最大值"].abs}.max
@record["计算结果"]["回程误差"] = @record["示值检定"].values.map {|e| e["回程误差最大值"].abs}.max

@record["计算结果"]["示值允差"] = ((@record["示值允差"]["测量范围上限"].to_f - @record["示值允差"]["测量范围下限"].to_f) *
                                   @record["示值允差"]["准确度等级"].to_f)/100.abs
@record["计算结果"]["回程允差"] = @record["计算结果"]["示值允差"].abs
@record["计算结果"]["稳定性允差"] = @record["计算结果"]["示值允差"].abs
@record["计算结果"]["静压零位允差"] = (@record["示值允差"]["准确度等级"].to_f).abs

@record
