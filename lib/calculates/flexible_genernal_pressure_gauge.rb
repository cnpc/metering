show_value_error = []
backbaul_value_error = []
@record["计算结果"]["轻敲位移"] = {}
@record["计算结果"]["轻敲位移"]["升压"] = []
@record["计算结果"]["轻敲位移"]["降压"] = []

@record["示值检定"]["标准器的压力值"].each_with_index do |value, index|
  show_value_error.push((@record["示值检定"]["轻敲表壳后被检仪表示值"]["升压"][index].to_f - value.to_f).round(4))
  backbaul_value_error.push((@record["示值检定"]["轻敲表壳后被检仪表示值"]["降压"][index].to_f - value.to_f).abs.round(4))
end

@record["计算结果"]["轻敲位移"]["升压"] = show_value_error
@record["计算结果"]["轻敲位移"]["降压"] = backbaul_value_error

@record["计算结果"]["最大允许误差"] = (((@record["测量范围上限"].to_f - @record["测量范围下限"].to_f) *
  @record["准确度等级"].to_f)/100).abs

#@record["计算结果"]["最大允许回差"] = @record["计算结果"]["示值允差"].abs
@record["计算结果"]["最大示值误差"] = show_value_error.max
@record["计算结果"]["最大回程误差"] = backbaul_value_error.max
@record
