result = @record["计算结果"]
record = {
  "外观检查" => result["外观检查"] || nil,
  "零位漂移" => result["零位漂移"] || nil,
  "示值误差" => result["示值误差"] || nil,
  "回程误差" => result["回程误差"] || nil,
  "静压零位误差" => result["静压零位误差"] || nil,
  "稳定性" => result["稳定性"] || nil,
  "绝缘电阻" => result["绝缘电阻"] || nil,
  "标准器示值" => @record["示值检定"].map{|k,v|  v['标准器示值']} || [nil]*11,
  "正行程平均值" => @record["示值检定"].map{|k,v|  v['正行程平均值']} || [nil]*11,
  "反行程平均值" => @record["示值检定"].map{|k,v|  v['反行程平均值']} || [nil]*11,
  "不合格项" => @record['result']['message'] || nil
}

record
