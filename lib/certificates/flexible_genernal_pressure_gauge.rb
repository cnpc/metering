result = @record["计算结果"]
record = {
  "外观检查" => result["外观检查"] || nil,
  "零位误差" => result["零位误差"] || nil,
  "示值误差" => result["最大示值误差"] || nil,
  "回程误差" => result["最大回程误差"] || nil,
  "轻敲位移" => result["最大回程误差"] || nil,
  "指针偏转平稳性" => result["指针偏转平稳性"] || nil,
  "其他" => result["其他"] || nil,
  "不合格项" => @record["result"]["message"] || nil
}
record
