#result = @record["计算结果"]
#record = {
  #"规格型号" => verification.instrument.specification || nil,
  #"测量范围" => verification.instrument.detection_range || nil,
  #"输出范围" => result["输出范围"] || nil,
  #"环境温度" => result["环境温度"] || nil,
  #"环境湿度" => result["环境湿度"] || nil,
  #"检定地点" => nil,
  #"外观检查" => result["外观检查"] || nil,
  #"密封性" => result["密封性"] || nil,
  #"绝缘电阻" => result["绝缘电阻"] || nil,
  #"绝缘强度" => result["绝缘强度"] || nil,
  #"允许误差" => result["允许误差"] || nil,
  #"最大误差" => result["最大误差"] || nil,
  #"允许回差" => result["允许回差"] || nil,
  #"最大回差" => result["最大回差"] || nil,
  #"静压影响" => result["静压影响"] || nil,
  #"不合格项" => @record['result']['message'] || nil
#}

{
  "规格型号" => nil,
  "测量范围" => nil,
  "输出范围" => nil,
  "环境温度" => nil,
  "环境湿度" => nil,
  "检定地点" => nil,
  "外观检查" => nil,
  "密封性" => nil,
  "绝缘电阻" => nil,
  "绝缘强度" => nil,
  "允许误差" => nil,
  "最大误差" => nil,
  "允许回差" => nil,
  "最大回差" => nil,
  "静压影响" => nil,
  "不合格项" => nil
}
