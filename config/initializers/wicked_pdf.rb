WickedPdf.config = {
  :wkhtmltopdf => '/usr/local/bin/wkhtmltopdf',
  :layout => "layouts/application.pdf.slim",
  :exe_path => '/usr/local/bin/wkhtmltopdf',
  :encoding => 'utf-8'
}
