module ExceptionNotifier
  class SlackNotifier

    attr_accessor :slack_options

    def initialize(options)
      self.slack_options = options
    end

    def call(exception, options={})
      env = options[:env]

      link = env['HTTP_HOST'] + env['REQUEST_URI']
      title = "#{env['REQUEST_METHOD']} <http://#{link}|http://#{link}>\n"

      message = "------------------------------------------------------------------------------------------\n"
      message += "*Environment:* #{Rails.env}\n"
      message += "*Time:* #{Time.zone.now.strftime('%Y-%m-%d %H:%M:%S')}\n"
      message += "*Exception:* `#{exception.message}`\n"

      req = Rack::Request.new(env)
      unless req.params.empty?
        message += "*Parameters:*\n"
        message += req.params.map { |k, v| ">#{k}=#{v}" }.join("\n")
        message += "\n"
      end
      message += "*Backtrace*: "
      message += "`#{exception.backtrace.first}`\n"
      message += "---------------  详细信息  ---------------\n"
      message += "#{exception.backtrace.take(5).join("\n")}"

      notifier = Slack::Notifier.new slack_options.fetch(:webhook_url),
                                     channel: slack_options.fetch(:channel),
                                     username: slack_options.fetch(:username),
                                     # icon_emoji: slack_options.fetch(:icon_emoji),
                                     attachments: [{
                                                       color: 'danger',
                                                       title: title,
                                                       text: message,
                                                       mrkdwn_in: %w(text title fallback)
                                                   }]
      notifier.ping ''
    end
  end
end
require 'exception_notification/rails'

ExceptionNotification.configure do |config|
  # Ignore additional exception types.
  # ActiveRecord::RecordNotFound, AbstractController::ActionNotFound and ActionController::RoutingError are already added.
  # config.ignored_exceptions += %w{ActionView::TemplateError CustomError}

  # Adds a condition to decide when an exception must be ignored or not.
  # The ignore_if method can be invoked multiple times to add extra conditions.
  config.ignore_if do |exception, options|
    not Rails.env.production?
  end

  # Notifiers =================================================================
  config.add_notifier :slack, {
    :webhook_url => "https://hooks.slack.com/services/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",
    :channel     => "#metering",
    :username => "incoming-metering"
  }

end
