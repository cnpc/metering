WdyMeasure::Application.routes.draw do

  root 'dashboard#index'

  # ------------- APIs -------------
  namespace :api, defaults: {format: :json} do
    resources :categories,       only: [:index, :show]
    resources :instrument_kinds, only: [:index, :show]

    resources :instruments,      only: [:create] do
      collection do
        post 'unique'
        post 'search'
      end
    end

    resources :verifications,    only: [:create]
    resources :organs,           only: [:index]
    resources :roles,            only: [:index]

    resources :users,            only: [:index, :create] do
      member do
        post 'approve'
      end
    end

    resources :early_warnings do
      collection do
        get "expires"
        get "scraps"
      end
    end
  end


  resources :notifications, only: [:index, :show]
  get "datalogs" => 'data_logs#index'
  resources :inspections

  resources :notices do
    member do
      get 'edit_inside_page'
      patch 'update_content'
    end
    collection do
      get 'search'
    end
  end

  # devise_for :users

  resources :users
  resources :settings do
    collection do
      get 'organ'
    end
  end

  resources :regulations
  resources :verifications do # 检测检定
    member do
      get 'report'
      get 'certificate'
      post 'calculate'
    end
    collection do
      get 'update_state'
      get 'organ'
      get 'imports'
      post 'process_import'
    end
  end
  resources :reports   # 检定数据报告（检定纪录）
  resources :organs    # 组织单位

  resources :categories do # 分类
    resources :category_types # 类型
    resources :category_ranges # 范围
    resources :category_precisions  # 精度
  end

  get 'instruments/search/:query', to: 'instruments#search', as: 'search'
  get 'instruments/search', to: 'instruments#search'
  resources :instruments do 
    collection do 
      get 'expires'
      get 'scraps'
    end
  end

  resources :vendors
  resources :tasks

  namespace :wdy do
    namespace :unirole do
      resources :organs
      resources :jobs
      resources :users
    end
  end

  resources :instrument_kinds

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
