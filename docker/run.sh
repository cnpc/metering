#!/bin/bash
# cd /opt

sed -i 's/PLACEHOLDER_DB_USER/'"$DB_ENV_POSTGRESQL_USER"'/' /opt/config/database.yml
sed -i 's/PLACEHOLDER_DB_PASS/'"$DB_ENV_POSTGRESQL_PASS"'/' /opt/config/database.yml
sed -i 's/PLACEHOLDER_DB_DATABASE/'"$DB_ENV_POSTGRESQL_DB"'/' /opt/config/database.yml
sed -i 's/PLACEHOLDER_DB_HOST/'"$DB_HOST"'/' /opt/config/database.yml
sed -i 's/PLACEHOLDER_ADMIN_USER/'"$ADMIN_USER"'/' /opt/app/controllers/cas_action_controller.rb

sed -i 's#PLACEHOLDER_SSO_URL#'"$SSO_CAS"'#' /opt/config/application.rb
sed -i 's#http://115.159.43.146:7080#'"$SSO_CAS"'#' /opt/config/application.rb
# export SECRET_KEY_BASE=`rake secret RAILS_ENV=production`
# bundle exec rake db:create

bundle exec rake db:schema:load
bundle exec rake db:migrate
bundle exec rake db:seed
bundle exec rake assets:precompile
bundle exec rake kindeditor:assets

bundle exec puma -C config/puma.conf

