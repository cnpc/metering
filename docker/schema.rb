Sequel.migration do
  change do
    create_table(:categories) do
      primary_key :id, :type=>"integer"
      column :name, "varchar(255)"
      column :organ_id, "integer"
      column :status, "integer", :default=>0
      column :created_at, "Timestamp"
      column :updated_at, "Timestamp"
    end

    create_table(:category_precisions) do
      primary_key :id, :type=>"integer"
      column :name, "varchar(255)"
      column :category_id, "integer"
      column :organ_id, "integer"
      column :status, "integer", :default=>0
      column :created_at, "Timestamp"
      column :updated_at, "Timestamp"
    end

    create_table(:category_ranges) do
      primary_key :id, :type=>"integer"
      column :start, "varchar(255)"
      column :end, "varchar(255)"
      column :unit, "varchar(255)"
      column :category_id, "integer"
      column :organ_id, "integer"
      column :status, "integer", :default=>0
      column :created_at, "Timestamp"
      column :updated_at, "Timestamp"
    end

    create_table(:category_types) do
      primary_key :id, :type=>"integer"
      column :name, "varchar(255)"
      column :category_id, "integer"
      column :organ_id, "integer"
      column :status, "integer", :default=>0
      column :created_at, "Timestamp"
      column :updated_at, "Timestamp"
    end

    create_table(:instruments) do
      primary_key :id, :type=>"integer"
      column :sn, "varchar(255)"
      column :specification, "varchar(255)"
      column :date, "date"
      column :vendor_id, "integer"
      column :category_precision_id, "integer"
      column :category_range_id, "integer"
      column :category_type_id, "integer"
      column :category_id, "integer"
      column :standard, "integer", :default=>0
      column :organ_id, "integer"
      column :status, "integer", :default=>0
      column :created_at, "Timestamp"
      column :updated_at, "Timestamp"
      column :name, "varchar(255)"
      column :basis, "varchar(255)"
      column :parent_id, "integer"
    end

    create_table(:organs) do
      primary_key :id, :type=>"integer"
      column :name, "varchar(255)"
    end

    create_table(:reports) do
      primary_key :id, :type=>"integer"
      column :verification_id, "integer"
      column :data, "text"
      column :verification_date, "date"
      column :checked_date, "date"
    end

    create_table(:role_names) do
      primary_key :id, :type=>"integer"
      column :name, "varchar(255)"
    end

    create_table(:roles) do
      primary_key :id, :type=>"integer"
      column :name, "varchar(255)"
    end

    create_table(:schema_migrations) do
      column :filename, "varchar(255)", :null=>false

      primary_key [:filename]
    end

    create_table(:settings) do
      primary_key :id, :type=>"integer"
      column :content, "text"
      column :name, "varchar(255)"
    end

    create_table(:tasks) do
      primary_key :id, :type=>"integer"
      column :sid, "varchar(255)"
      column :name, "varchar(255)"
      column :formkey, "varchar(255)"
      column :title, "varchar(255)"
      column :descr, "varchar(255)"
      column :who, "varchar(255)"
      column :created_at, "Timestamp"
      column :params, "varchar(255)"
      column :state, "varchar(255)"
    end

    create_table(:users) do
      primary_key :id, :type=>"integer"
      column :email, "varchar(255)", :default=>"", :null=>false
      column :encrypted_password, "varchar(255)", :default=>"", :null=>false
      column :reset_password_token, "varchar(255)"
      column :reset_password_sent_at, "varchar(255)"
      column :remember_created_at, "Timestamp"
      column :sign_in_count, "integer", :default=>0, :null=>false
      column :current_sign_in_at, "Timestamp"
      column :last_sign_in_at, "Timestamp"
      column :current_sign_in_ip, "varchar(255)"
      column :last_sign_in_ip, "varchar(255)"
      column :created_at, "Timestamp"
      column :updated_at, "Timestamp"
      column :username, "varchar(255)"
      column :organ_id, "integer"
      column :role_id, "integer"

      index [:email], :unique=>true
      index [:reset_password_token], :unique=>true
    end

    create_table(:vendors) do
      primary_key :id, :type=>"integer"
      column :name, "varchar(255)"
      column :nickname, "varchar(255)"
      column :pinyin, "varchar(255)"
      column :contact, "varchar(255)"
      column :phone, "integer"
      column :organ_id, "integer"
      column :status, "integer", :default=>0
      column :created_at, "Timestamp"
      column :updated_at, "Timestamp"
    end

    create_table(:verifications) do
      primary_key :id, :type=>"integer"
      column :sn, "varchar(255)"
      column :organ_id, "integer"
      column :date, "date"
      column :status, "integer", :default=>0
      column :state, "varchar(255)"
      column :created_at, "Timestamp"
      column :updated_at, "Timestamp"
      column :report_id, "integer"
      column :instrument_id, "integer"
      column :standard_id, "integer"
    end
  end
end
Sequel.migration do
  change do
    self << "INSERT INTO `schema_migrations` (`filename`) VALUES ('20150518235505_create_reports.rb')"
    self << "INSERT INTO `schema_migrations` (`filename`) VALUES ('20150519032142_add_report_id_to_verifications.rb')"
    self << "INSERT INTO `schema_migrations` (`filename`) VALUES ('20150519095555_add_instrument_id_to_verifications.rb')"
    self << "INSERT INTO `schema_migrations` (`filename`) VALUES ('20150522091253_add_name_to_instruments.rb')"
    self << "INSERT INTO `schema_migrations` (`filename`) VALUES ('20150529062201_devise_create_users.rb')"
    self << "INSERT INTO `schema_migrations` (`filename`) VALUES ('20150529121718_create_role_names.rb')"
    self << "INSERT INTO `schema_migrations` (`filename`) VALUES ('20150529122233_create_roles.rb')"
    self << "INSERT INTO `schema_migrations` (`filename`) VALUES ('20150601103643_add_username_to_users.rb')"
    self << "INSERT INTO `schema_migrations` (`filename`) VALUES ('20150601122809_create_organs.rb')"
    self << "INSERT INTO `schema_migrations` (`filename`) VALUES ('20150601123701_add_organ_id_to_users.rb')"
    self << "INSERT INTO `schema_migrations` (`filename`) VALUES ('20150602045521_add_role_name_to_roles.rb')"
    self << "INSERT INTO `schema_migrations` (`filename`) VALUES ('20150602050141_remove_role_name_id_in_roles.rb')"
    self << "INSERT INTO `schema_migrations` (`filename`) VALUES ('20150602051202_remove_user_id_in_roles.rb')"
    self << "INSERT INTO `schema_migrations` (`filename`) VALUES ('20150602051323_add_role_id_to_users.rb')"
    self << "INSERT INTO `schema_migrations` (`filename`) VALUES ('20150603031257_create_settings.rb')"
    self << "INSERT INTO `schema_migrations` (`filename`) VALUES ('20150603032059_add_name_to_settings.rb')"
    self << "INSERT INTO `schema_migrations` (`filename`) VALUES ('20150603043302_change_date_type_in_instruments.rb')"
    self << "INSERT INTO `schema_migrations` (`filename`) VALUES ('20150612144932_add_basis_to_instruments.rb')"
    self << "INSERT INTO `schema_migrations` (`filename`) VALUES ('20150706111056_add_parent_id_to_instruments.rb')"
    self << "INSERT INTO `schema_migrations` (`filename`) VALUES ('20150707010033_add_standard_id_to_verifications.rb')"
    self << "INSERT INTO `schema_migrations` (`filename`) VALUES ('20150714030402_add_verification_date_to_reports.rb')"
    self << "INSERT INTO `schema_migrations` (`filename`) VALUES ('20150714045404_add_check_date_to_reports.rb')"
  end
end
