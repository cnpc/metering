'use strict';

// --------- 预警列表(过期,报废) ----------
angular.module('ealy_warning_app', ['ngResource', 'ui.router', 'ui.router.state'])
    .factory('EalyWarningRest', function($resource) {
        return $resource("/api/early_warnings/expires", {}, {
            expires: {method: "get", params: {action: "expires"}, isArray: true}
        })
    })
    .run(function($rootScope, $state) {})

    // =============================================================================
    // 应用环境
    // =============================================================================
    .config(function($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.when("", "/expires");
        $urlRouterProvider.otherwise("/expires");


        $stateProvider
            .state("expires", {
                url: "/expires",
                templateUrl: "/cloap/early_warnings/expires.html",
                controller: function($scope, $state, EalyWarningRest) {
                    $scope.expires = [];
                    EalyWarningRest.expires({}, function(data) {
                        $scope.expires = data;
                    });
                }
            })

    });

angular.module('ealy_warning_scrap_app', ['ngResource', 'ui.router', 'ui.router.state'])
    .factory('EalyWarningRest', function($resource) {
        return $resource("/api/early_warnings/scraps", {}, {
            scraps: {method: "get", params: {action: "scraps"}, isArray: true}
        })
    })
    .run(function($rootScope, $state) {})
    
    .config(function($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.when("", "/scraps");
        $urlRouterProvider.otherwise("/scraps");


        $stateProvider
            .state("scraps", {
                url: "/scraps",
                templateUrl: "/cloap/early_warnings/scraps.html",
                controller: function($scope, $state, EalyWarningRest) {
                    $scope.scraps = [];
                    EalyWarningRest.scraps({}, function(data) {
                        $scope.scraps = data;
                    });
                }
            })

    });


// --------- 检定任务 ----------
angular.module('verification_app', ['ngResource', 'ui.router', 'ui.router.state'])
    .factory('CategoryRest', function($resource) {
        return $resource("/api/categories/:id", {}, {});
    })
    .factory('DeviceTypeRest', function($resource) {
        return $resource("/api/instrument_kinds/:id", {}, {});
    })
    .factory('VerificationRest', function($resource) {
        return $resource("/api/verifications/:id", {}, {});
    })
    .factory('InstrumentRest', function($resource) {
        return $resource("/api/instruments/:id", {id: "@id"}, {
            unique: {method: "POST", params: {id: "unique"}, isArray: false}
        });
    })
    .run(function($rootScope, $state) {})

    // =============================================================================
    // 应用环境
    // =============================================================================
    .config(function($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.when("", "/");
        $urlRouterProvider.otherwise("/");


        $stateProvider
            .state("verifications", {
                url: "/",
                templateUrl: "/cloap/verification/device.html",
                controller: function($scope, $state, DeviceTypeRest, InstrumentRest) {
                    $scope.kinds  = [];
                    DeviceTypeRest.query({}, function(data) {
                        $scope.kinds = data;
                    });

                    $scope.is_register = false;
                    $scope.$on("verification.registering", function() {
                        $scope.is_register = true;
                    })
                    $scope.$on("verification.register.cancel", function() {
                        $scope.is_register = false;
                    })

                    $scope.next = function(dev_type_id, sn) {
                        InstrumentRest.unique({kind_id: dev_type_id, sn: sn}, function(data) {
                            $state.go("verifications.show", {kind_id: dev_type_id, sn: sn, instrument: data});
                        });
                    };
                }
            })
            .state("verifications.show", {
                url: "/:kind_id/:sn",
                params: {instrument: true},
                templateUrl: "/cloap/verification/register.html",
                controller: function($scope, $state, DeviceTypeRest, VerificationRest) {
                    var resp = $state.params.instrument;
                    $scope.elem = (resp.result == 'failure') ? resp.params : resp;
                    $scope.is_existed = (resp.result != 'failure');

                    $scope.$emit("verification.registering");


                    var today = function() {
                        var date = new Date();
                        var day = date.getDate();
                        var month = date.getMonth() + 1;
                        var year = date.getFullYear();

                        return (year + '-' + month + '-' + day);
                    };
                    $scope.elem.verification = {date: today()};

                    $scope.kind = {}
                    DeviceTypeRest.get({id: $state.params.kind_id}, function(data) {
                        $scope.kind = data;
                    });

                    $scope.submit = function(vf) {
                        vf.instrument_kind_id    = $scope.kind.id;
                        vf.category_id           = $scope.kind.category_id;
                        vf.verification.organ_id = vf.organ_id;
                        VerificationRest.save({data: vf}, function(data) {
                            window.location.href = "/";
                            // $state.go("categories");
                        })
                    };

                    $scope.cancel = function() {
                        $scope.$emit("verification.register.cancel");
                        $state.go("verifications");
                    }
                }
            })

    });





// --------- 新增仪器 ----------
angular.module('instrument_app', ['ngResource', 'ui.router', 'ui.router.state'])
    .factory('DeviceTypeRest', function($resource) {
        return $resource("/api/instrument_kinds/:id", {}, {});
    })
    .factory('OrganRest', function($resource) {
        return $resource("/api/organs/:id", {}, {});
    })
    .factory('InstrumentRest', function($resource) {
        return $resource("/api/instruments/:id", {}, {})
    })
    .run(function($rootScope, $state) {})

    // =============================================================================
    // 应用环境
    // =============================================================================
    .config(function($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.when("", "/device");
        $urlRouterProvider.otherwise("/device");


        $stateProvider
            .state("device", {
                url: "/device",
                templateUrl: "/cloap/instrument/new.html",
                controller: function($scope, $state, DeviceTypeRest, InstrumentRest, OrganRest) {
                    $scope.kinds  = [];
                    $scope.kind   = {};
                    $scope.organs = [];

                    DeviceTypeRest.query({}, function(data) {
                        $scope.kinds = data;
                    });

                    OrganRest.query({}, function(data) {
                        $scope.organs = data;
                    });

                    $scope.next = function(id) {
                        DeviceTypeRest.get({id: id}, function(data) {
                            $scope.kind = data;
                        })
                    };

                    $scope.submit = function(vf) {
                        console.log(vf);
                        vf.category_id = $scope.kind.category_id;
                        InstrumentRest.save({data: vf}, function(data) {
                            console.log(data);
                            $scope.kind = {};
                            // window.location.href = "/";
                            // $state.go("device");
                        });
                    }
                }
            })

    });







// --------- 注册用户 ----------
angular.module('user_register_app', ['ngResource', 'ui.router', 'ui.router.state'])
    .factory('OrganRest', function($resource) {
        return $resource("/api/organs/:id", {}, {});
    })
    .factory('RoleRest', function($resource) {
        return $resource("/api/roles/:id", {}, {})
    })
    .factory('UserRest', function($resource) {
        return $resource("/api/users/:action", {}, {
            approve: {method: "post", params: {action: "approve"}, isArray: false}
        })
    })
    .run(function($rootScope, $state) {})

    // =============================================================================
    // 应用环境
    // =============================================================================
    .config(function($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.when("", "/");
        $urlRouterProvider.otherwise("/");


        $stateProvider
            .state("user_register", {
                url: "/",
                templateUrl: "/cloap/user/register.html",
                controller: function($scope, $state, OrganRest, RoleRest, UserRest) {
                    $scope.organs  = [];
                    $scope.roles   = [];

                    OrganRest.query({}, function(data) {
                        $scope.organs = data;
                    });

                    RoleRest.query({}, function(data) {
                        $scope.roles = data;
                    });

                    $scope.submit = function(vf) {
                        UserRest.save(vf, function(data) {
                            window.location.href = "/";
                        });
                    }
                }
            })

    });






// --------- 用户状态管理 ----------
angular.module('user_state_app', ['ngResource', 'ui.router', 'ui.router.state'])
    .factory('UserRest', function($resource) {
        return $resource("/api/users/:id/:action", {id: "@id"}, {
            approve: {method: "post", params: {action: "approve"}, isArray: false}
        })
    })
    .run(function($rootScope, $state) {})

    // =============================================================================
    // 应用环境
    // =============================================================================
    .config(function($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.when("", "/");
        $urlRouterProvider.otherwise("/");


        $stateProvider
            .state("user", {
                url: "/",
                templateUrl: "/cloap/user/index.html",
                controller: function($scope, $state, UserRest) {
                    $scope.users = [];

                    UserRest.query({}, function(data) {
                        $scope.users = data;
                    });

                    $scope.approve = function(idx, uid) {
                        UserRest.approve({id: uid}, function(data) {
                            $scope.users = _.map($scope.users, function(e) {
                                if (e.username == uid) {
                                    return data;
                                }
                                return e;
                            });
                        });
                    }
                }
            })

    });









// ----------------------------------------------
// 计量仪器查询
// ----------------------------------------------
angular.module('instrument_search_app', ['ngResource', 'ui.router', 'ui.router.state'])
    .factory('CategoryRest', function($resource) {
        return $resource("/api/categories/:id", {}, {});
    })
    .factory('DeviceTypeRest', function($resource) {
        return $resource("/api/instrument_kinds/:id", {}, {});
    })
    .factory('InstrumentRest', function($resource) {
        return $resource("/api/instruments/:id", {id: "@id"}, {
            search: {method: "POST", params: {id: "search"}, isArray: true}
        });
    })
    .factory('OrganRest', function($resource) {
        return $resource("/api/organs/:id", {}, {});
    })
    .run(function($rootScope, $state) {})

    // =============================================================================
    // 应用环境
    // =============================================================================
    .config(function($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.when("", "/");
        $urlRouterProvider.otherwise("/");


        $stateProvider
            .state("instrument_search", {
                url: "/",
                templateUrl: "/cloap/instrument/search.html",
                controller: function($scope, $state, CategoryRest, DeviceTypeRest, OrganRest, InstrumentRest) {
                    $scope.is_searched = false;
                    $scope.is_show_search = true;
                    $scope.categories = $scope.kinds = $scope.ranges = $scope.precisions = $scope.vendors = [];

                    OrganRest.query({}, function(data) { $scope.organs = data; });
                    CategoryRest.query({}, function(data) { $scope.categories = data; });

                    $scope.category = function(id) {
                        DeviceTypeRest.query({}, function(data) { $scope.kinds = _.filter(data, function(e) {return e.category_id == id;}); });
                    };

                    $scope.instrument_kind = function(id) {
                        DeviceTypeRest.get({id: id}, function(data) {
                            $scope.ranges = data.ranges;
                            $scope.precisions = data.precisions;
                            $scope.vendors = data.vendors;
                        });
                    };

                    $scope.toggle = function() {
                        $scope.is_show_search = ! $scope.is_show_search;
                    };

                    $scope.submit = function(elem) {
                        InstrumentRest.search(elem, function(data) {
                            console.log(data);

                            $scope.is_searched = true;
                            $scope.is_show_search = false;
                            $scope.instruments = data;
                        });
                    }
                }
            })

    });
