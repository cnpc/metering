# -*- encoding: utf-8 -*-
# stub: orm_adapter-sequel 0.1.0 ruby lib

Gem::Specification.new do |s|
  s.name = "orm_adapter-sequel"
  s.version = "0.1.0"

  s.required_rubygems_version = Gem::Requirement.new(">= 1.3.6") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Don Morrison"]
  s.date = "2013-03-20"
  s.description = "Adds Sequel ORM adapter to the orm_adapter project"
  s.email = "elskwid@gmail.com"
  s.homepage = "http://github.com/elskwid/orm_adapter-sequel"
  s.rubyforge_project = "orm_adapter-sequel"
  s.rubygems_version = "2.4.6"
  s.summary = "Adds sequel adapter to orm_adapter which provides a single point of entry for using basic features of popular ruby ORMs."

  s.installed_by_version = "2.4.6" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 3

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<activemodel>, [">= 3.0.0"])
      s.add_runtime_dependency(%q<orm_adapter>, [">= 0.4.0"])
      s.add_runtime_dependency(%q<sequel>, [">= 3.18.0"])
      s.add_development_dependency(%q<bundler>, [">= 1.0.0"])
      s.add_development_dependency(%q<git>, [">= 1.2.5"])
      s.add_development_dependency(%q<rake>, [">= 0.8.7"])
      s.add_development_dependency(%q<rspec>, [">= 2.4.0"])
      s.add_development_dependency(%q<sqlite3-ruby>, [">= 1.3.2"])
      s.add_development_dependency(%q<yard>, [">= 0.6.0"])
    else
      s.add_dependency(%q<activemodel>, [">= 3.0.0"])
      s.add_dependency(%q<orm_adapter>, [">= 0.4.0"])
      s.add_dependency(%q<sequel>, [">= 3.18.0"])
      s.add_dependency(%q<bundler>, [">= 1.0.0"])
      s.add_dependency(%q<git>, [">= 1.2.5"])
      s.add_dependency(%q<rake>, [">= 0.8.7"])
      s.add_dependency(%q<rspec>, [">= 2.4.0"])
      s.add_dependency(%q<sqlite3-ruby>, [">= 1.3.2"])
      s.add_dependency(%q<yard>, [">= 0.6.0"])
    end
  else
    s.add_dependency(%q<activemodel>, [">= 3.0.0"])
    s.add_dependency(%q<orm_adapter>, [">= 0.4.0"])
    s.add_dependency(%q<sequel>, [">= 3.18.0"])
    s.add_dependency(%q<bundler>, [">= 1.0.0"])
    s.add_dependency(%q<git>, [">= 1.2.5"])
    s.add_dependency(%q<rake>, [">= 0.8.7"])
    s.add_dependency(%q<rspec>, [">= 2.4.0"])
    s.add_dependency(%q<sqlite3-ruby>, [">= 1.3.2"])
    s.add_dependency(%q<yard>, [">= 0.6.0"])
  end
end
