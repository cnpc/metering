# -*- encoding: utf-8 -*-
# stub: sequel-rails 0.9.9 ruby lib

Gem::Specification.new do |s|
  s.name = "sequel-rails"
  s.version = "0.9.9"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Brasten Sager (brasten)", "Jonathan TRON"]
  s.date = "2015-01-04"
  s.description = "Integrate Sequel with Rails (3.x and 4.x)"
  s.email = ["brasten@gmail.com", "jonathan.tron@metrilio.com"]
  s.extra_rdoc_files = ["LICENSE", "README.md"]
  s.files = ["LICENSE", "README.md"]
  s.homepage = "http://talentbox.github.io/sequel-rails/"
  s.licenses = ["MIT"]
  s.rdoc_options = ["--charset=UTF-8"]
  s.rubygems_version = "2.4.6"
  s.summary = "Use Sequel with Rails (3.x and 4.x)"

  s.installed_by_version = "2.4.6" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<activemodel>, [">= 0"])
      s.add_runtime_dependency(%q<railties>, [">= 3.2.0"])
      s.add_runtime_dependency(%q<actionpack>, [">= 3.2.0"])
      s.add_runtime_dependency(%q<sequel>, ["< 5.0", ">= 3.28"])
      s.add_development_dependency(%q<combustion>, [">= 0"])
      s.add_development_dependency(%q<generator_spec>, [">= 0"])
      s.add_development_dependency(%q<rake>, [">= 0.8.7"])
      s.add_development_dependency(%q<rspec>, ["~> 3.1"])
      s.add_development_dependency(%q<rspec-rails>, ["~> 3.1"])
      s.add_development_dependency(%q<rubocop>, ["~> 0.28.0"])
      s.add_development_dependency(%q<ammeter>, ["= 1.1.2"])
      s.add_development_dependency(%q<test-unit>, [">= 0"])
    else
      s.add_dependency(%q<activemodel>, [">= 0"])
      s.add_dependency(%q<railties>, [">= 3.2.0"])
      s.add_dependency(%q<actionpack>, [">= 3.2.0"])
      s.add_dependency(%q<sequel>, ["< 5.0", ">= 3.28"])
      s.add_dependency(%q<combustion>, [">= 0"])
      s.add_dependency(%q<generator_spec>, [">= 0"])
      s.add_dependency(%q<rake>, [">= 0.8.7"])
      s.add_dependency(%q<rspec>, ["~> 3.1"])
      s.add_dependency(%q<rspec-rails>, ["~> 3.1"])
      s.add_dependency(%q<rubocop>, ["~> 0.28.0"])
      s.add_dependency(%q<ammeter>, ["= 1.1.2"])
      s.add_dependency(%q<test-unit>, [">= 0"])
    end
  else
    s.add_dependency(%q<activemodel>, [">= 0"])
    s.add_dependency(%q<railties>, [">= 3.2.0"])
    s.add_dependency(%q<actionpack>, [">= 3.2.0"])
    s.add_dependency(%q<sequel>, ["< 5.0", ">= 3.28"])
    s.add_dependency(%q<combustion>, [">= 0"])
    s.add_dependency(%q<generator_spec>, [">= 0"])
    s.add_dependency(%q<rake>, [">= 0.8.7"])
    s.add_dependency(%q<rspec>, ["~> 3.1"])
    s.add_dependency(%q<rspec-rails>, ["~> 3.1"])
    s.add_dependency(%q<rubocop>, ["~> 0.28.0"])
    s.add_dependency(%q<ammeter>, ["= 1.1.2"])
    s.add_dependency(%q<test-unit>, [">= 0"])
  end
end
