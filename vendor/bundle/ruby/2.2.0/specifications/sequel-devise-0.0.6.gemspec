# -*- encoding: utf-8 -*-
# stub: sequel-devise 0.0.6 ruby lib

Gem::Specification.new do |s|
  s.name = "sequel-devise"
  s.version = "0.0.6"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Rodrigo Rosenfeld Rosas"]
  s.date = "2014-10-22"
  s.description = "Devise support for Sequel models"
  s.email = ["rr.rosas@gmail.com"]
  s.homepage = "https://github.com/rosenfeld/sequel-devise"
  s.rubygems_version = "2.4.6"
  s.summary = "Enable Devise support by adding plugin :devise to your Sequel Model"

  s.installed_by_version = "2.4.6" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<devise>, [">= 0"])
      s.add_runtime_dependency(%q<orm_adapter-sequel>, [">= 0"])
    else
      s.add_dependency(%q<devise>, [">= 0"])
      s.add_dependency(%q<orm_adapter-sequel>, [">= 0"])
    end
  else
    s.add_dependency(%q<devise>, [">= 0"])
    s.add_dependency(%q<orm_adapter-sequel>, [">= 0"])
  end
end
