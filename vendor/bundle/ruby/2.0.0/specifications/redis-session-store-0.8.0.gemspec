# -*- encoding: utf-8 -*-
# stub: redis-session-store 0.8.0 ruby lib

Gem::Specification.new do |s|
  s.name = "redis-session-store"
  s.version = "0.8.0"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Mathias Meyer"]
  s.date = "2014-08-28"
  s.description = "A drop-in replacement for e.g. MemCacheStore to store Rails sessions (and Rails sessions only) in Redis."
  s.email = ["meyer@paperplanes.de"]
  s.extra_rdoc_files = ["LICENSE", "AUTHORS.md", "CONTRIBUTING.md"]
  s.files = ["AUTHORS.md", "CONTRIBUTING.md", "LICENSE"]
  s.homepage = "https://github.com/roidrage/redis-session-store"
  s.licenses = ["MIT"]
  s.rubygems_version = "2.4.8"
  s.summary = "A drop-in replacement for e.g. MemCacheStore to store Rails sessions (and Rails sessions only) in Redis."

  s.installed_by_version = "2.4.8" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<redis>, [">= 0"])
      s.add_runtime_dependency(%q<actionpack>, ["< 5", ">= 3"])
      s.add_development_dependency(%q<fakeredis>, [">= 0"])
      s.add_development_dependency(%q<rake>, [">= 0"])
      s.add_development_dependency(%q<rspec>, [">= 0"])
      s.add_development_dependency(%q<rubocop>, [">= 0"])
      s.add_development_dependency(%q<simplecov>, [">= 0"])
    else
      s.add_dependency(%q<redis>, [">= 0"])
      s.add_dependency(%q<actionpack>, ["< 5", ">= 3"])
      s.add_dependency(%q<fakeredis>, [">= 0"])
      s.add_dependency(%q<rake>, [">= 0"])
      s.add_dependency(%q<rspec>, [">= 0"])
      s.add_dependency(%q<rubocop>, [">= 0"])
      s.add_dependency(%q<simplecov>, [">= 0"])
    end
  else
    s.add_dependency(%q<redis>, [">= 0"])
    s.add_dependency(%q<actionpack>, ["< 5", ">= 3"])
    s.add_dependency(%q<fakeredis>, [">= 0"])
    s.add_dependency(%q<rake>, [">= 0"])
    s.add_dependency(%q<rspec>, [">= 0"])
    s.add_dependency(%q<rubocop>, [">= 0"])
    s.add_dependency(%q<simplecov>, [">= 0"])
  end
end
