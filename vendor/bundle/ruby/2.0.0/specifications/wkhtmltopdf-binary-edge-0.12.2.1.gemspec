# -*- encoding: utf-8 -*-
# stub: wkhtmltopdf-binary-edge 0.12.2.1 ruby lib

Gem::Specification.new do |s|
  s.name = "wkhtmltopdf-binary-edge"
  s.version = "0.12.2.1"

  s.required_rubygems_version = Gem::Requirement.new(">= 1.3.6") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Orginial Author: - Research Information Systems, The University of Iowa", "Yurui Zhang"]
  s.date = "2015-02-16"
  s.description = "Includes wkthmltopdf binaires for MacOSX and Ubuntu 14(32bit & 64bit)"
  s.email = "yuruiology@gmail.com, vpr-ris-developers@iowa.uiowa.edu,mcollas@aconex.com"
  s.executables = ["wkhtmltopdf"]
  s.files = ["bin/wkhtmltopdf"]
  s.homepage = "https://github.com/pallymore/wkhtmltopdf-binary-edge"
  s.required_ruby_version = Gem::Requirement.new(">= 1.8.7")
  s.rubygems_version = "2.4.8"
  s.summary = "Provides binaries for WKHTMLTOPDF project in an easily accessible package. - forked from wkhtmltopdf-binary"

  s.installed_by_version = "2.4.8" if s.respond_to? :installed_by_version
end
