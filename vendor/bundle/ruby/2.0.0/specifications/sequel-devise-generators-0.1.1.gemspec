# -*- encoding: utf-8 -*-
# stub: sequel-devise-generators 0.1.1 ruby lib

Gem::Specification.new do |s|
  s.name = "sequel-devise-generators"
  s.version = "0.1.1"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Michael Sell"]
  s.date = "2014-05-26"
  s.description = "Provides the Devise generators for sequel-devise and sequel-rails. Similar to the default ActiveRecord generators."
  s.email = ["mdsell@gmail.com"]
  s.homepage = ""
  s.licenses = ["MIT"]
  s.rubygems_version = "2.4.8"
  s.summary = "Rails generators for sequel-devise"

  s.installed_by_version = "2.4.8" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 3

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<sequel-devise>, [">= 0.0.3"])
      s.add_runtime_dependency(%q<sequel-rails>, [">= 0.9.1"])
      s.add_runtime_dependency(%q<rails>, [">= 3.2"])
      s.add_development_dependency(%q<bundler>, ["~> 1.5"])
      s.add_development_dependency(%q<rake>, [">= 0"])
    else
      s.add_dependency(%q<sequel-devise>, [">= 0.0.3"])
      s.add_dependency(%q<sequel-rails>, [">= 0.9.1"])
      s.add_dependency(%q<rails>, [">= 3.2"])
      s.add_dependency(%q<bundler>, ["~> 1.5"])
      s.add_dependency(%q<rake>, [">= 0"])
    end
  else
    s.add_dependency(%q<sequel-devise>, [">= 0.0.3"])
    s.add_dependency(%q<sequel-rails>, [">= 0.9.1"])
    s.add_dependency(%q<rails>, [">= 3.2"])
    s.add_dependency(%q<bundler>, ["~> 1.5"])
    s.add_dependency(%q<rake>, [">= 0"])
  end
end
