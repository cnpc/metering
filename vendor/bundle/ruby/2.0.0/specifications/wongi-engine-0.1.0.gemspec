# -*- encoding: utf-8 -*-
# stub: wongi-engine 0.1.0 ruby lib

Gem::Specification.new do |s|
  s.name = "wongi-engine"
  s.version = "0.1.0"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Valeri Sokolov"]
  s.date = "2015-02-21"
  s.description = "A rule engine."
  s.email = ["ulfurinn@ulfurinn.net"]
  s.homepage = "https://github.com/ulfurinn/wongi-engine"
  s.licenses = ["MIT"]
  s.rubygems_version = "2.4.8"
  s.summary = "A forward-chaining rule engine in pure Ruby."

  s.installed_by_version = "2.4.8" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_development_dependency(%q<rake>, ["~> 10.0"])
      s.add_development_dependency(%q<rspec>, ["~> 3.1"])
      s.add_development_dependency(%q<rspec-collection_matchers>, ["~> 1.1"])
    else
      s.add_dependency(%q<rake>, ["~> 10.0"])
      s.add_dependency(%q<rspec>, ["~> 3.1"])
      s.add_dependency(%q<rspec-collection_matchers>, ["~> 1.1"])
    end
  else
    s.add_dependency(%q<rake>, ["~> 10.0"])
    s.add_dependency(%q<rspec>, ["~> 3.1"])
    s.add_dependency(%q<rspec-collection_matchers>, ["~> 1.1"])
  end
end
