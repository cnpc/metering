Sequel.migration do
  change do
    alter_table :instruments do
      drop_column :basis
    end
  end
end
