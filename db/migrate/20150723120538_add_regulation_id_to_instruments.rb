Sequel.migration do
  change do
    alter_table :instruments do
      add_column :regulation_id, Integer
    end
  end
end
