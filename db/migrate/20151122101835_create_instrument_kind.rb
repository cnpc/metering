Sequel.migration do
  change do
    create_table :instrument_kinds do
      primary_key :id
      String :sn
      String :name
      Integer :category_type_id
      Text :accuracy
      Text :regulation
    end
  end
end
