Sequel.migration do
  change do
    alter_table :notifications do
      add_column :created_at, DateTime
    end
  end
end
