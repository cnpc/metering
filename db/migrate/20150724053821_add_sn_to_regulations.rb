Sequel.migration do
  change do
    alter_table :regulations do
      add_column :sn, String
      add_column :category_id, Integer
      add_column :published_date, Date
      add_column :material_date, Date
    end
  end
end
