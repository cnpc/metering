Sequel.migration do
  change do
    alter_table :notices do
      add_column :expiried, TrueClass, default: false
    end
  end
end
