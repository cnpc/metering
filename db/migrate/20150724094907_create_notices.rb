Sequel.migration do 
  change do

    create_table :notices do
      primary_key :id
      String :sn
      Integer :organ_id
      Integer :instrument_id
      String :conclousion
      String :approver
      String :verifier
      String :inspector
      Date :verification_date
      Date :expiry_date
      Text :content
    end

  end
end