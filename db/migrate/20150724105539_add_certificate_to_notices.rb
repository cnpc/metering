Sequel.migration do
  change do
    alter_table :notices do
      add_column :certificate, Integer
      set_column_default :certificate, 0
    end
  end
end
