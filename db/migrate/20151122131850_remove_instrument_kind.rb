Sequel.migration do
  up do
    alter_table :instrument_kinds do
      drop_column :category_type_id
    end
  end

end
