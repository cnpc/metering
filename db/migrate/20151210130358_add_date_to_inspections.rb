Sequel.migration do
  change do
    alter_table :inspections do
      add_column :date, Date
    end
  end
end
