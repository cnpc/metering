Sequel.migration do
  change do
    alter_table :data_logs do
      add_column :notice_id, Integer
    end

    add_index :data_logs, :notice_id
  end
end
