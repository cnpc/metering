Sequel.migration do
  change do
    alter_table :inspections do
      add_column :category, String
    end
  end
end
