Sequel.migration do 
  change do

    create_table :regulations do
      primary_key :id
      String :name
    end

  end
end