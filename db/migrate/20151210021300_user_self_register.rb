# --------------------------------------------------------------------
# 用户自己注册自己的信息，需增加2个字段
# --------------------------------------------------------------------
# realname: 真实姓名
# state: 状态机字段
#   - approved     已批准的
#   - unauthorized 未授权的
#   - unknown      未知的
# 状态变化： unknown -- [Action: register] --> unauthorized -- [Action: approve] --> approved
# --------------------------------------------------------------------

Sequel.migration do
  change do
    alter_table :users do
      add_column :realname, String
      add_column :state, String
    end
  end
end
