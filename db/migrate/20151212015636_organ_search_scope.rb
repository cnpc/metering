Sequel.migration do
  change do
    alter_table :organs do
      add_column :scope, String, default: 'SELF'
    end
  end
end
