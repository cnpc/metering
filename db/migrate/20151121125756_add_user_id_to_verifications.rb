Sequel.migration do
  change do
    alter_table :verifications do
      add_column :user_id, Integer
    end
  end
end
