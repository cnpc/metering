Sequel.migration do
  change do
    alter_table :regulations do
      add_column :instrument_kind_id, Integer
    end
  end
end
