Sequel.migration do 
  change do

    create_table :notifications do
      primary_key :id
      Integer :organ_id
      Integer :user_id
      Integer :notice_id
      TrueClass :checked, default: false
      String :content
    end

  end
end
