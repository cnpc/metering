Sequel.migration do 
  change do

    create_table :inspections do
      primary_key :id
      String :title
      Text :content
      String :attachment
    end

  end
end