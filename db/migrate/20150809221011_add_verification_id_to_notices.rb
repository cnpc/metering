Sequel.migration do
  change do
    alter_table :notices do
      add_column :verification_id, Integer
    end
  end
end
