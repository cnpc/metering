Sequel.migration do 
  change do

    create_table :data_logs do
      primary_key :id
      Integer :user_id
      DateTime :created_at
      Text :content
    end

    add_index :data_logs, :id
  end
end
