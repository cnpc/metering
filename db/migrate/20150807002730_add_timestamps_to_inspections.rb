Sequel.migration do
  change do
    alter_table :inspections do
      add_column :created_at, DateTime, default: Time.now
      add_column :updated_at, DateTime, default: Time.now
    end
  end
end
