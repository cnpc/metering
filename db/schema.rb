Sequel.migration do
  change do
    create_table(:categories) do
      primary_key :id
      column :name, "varchar(255)"
      column :organ_id, "integer"
      column :status, "integer", :default=>0
      column :created_at, "timestamp"
      column :updated_at, "timestamp"
    end

    # create_table(:category_precisions) do
    #   primary_key :id
    #   column :name, "varchar(255)"
    #   column :category_id, "integer"
    #   column :organ_id, "integer"
    #   column :status, "integer", :default=>0
    #   column :created_at, "timestamp"
    #   column :updated_at, "timestamp"
    # end

    # create_table(:category_ranges) do
    #   primary_key :id
    #   column :start, "integer"
    #   column :end, "integer"
    #   column :unit, "varchar(255)"
    #   column :category_id, "integer"
    #   column :organ_id, "integer"
    #   column :status, "integer", :default=>0
    #   column :created_at, "timestamp"
    #   column :updated_at, "timestamp"
    # end

    # create_table(:category_types) do
    #   primary_key :id
    #   column :name, "varchar(255)"
    #   column :category_id, "integer"
    #   column :created_at, "timestamp"
    #   column :updated_at, "timestamp"
    # end

    create_table(:instruments) do
      primary_key :id
      column :sn, "varchar(128)"
      column :specification, "varchar(128)"
      column :date, "date"
      column :vendor_id, "integer"
      column :precision, "varchar(128)"
      column :range, "varchar(128)"
      column :instrument_kind_id, "integer"
      column :category_id, "integer"
      column :standard, "integer", :default=>0
      column :organ_id, "integer"
      column :status, "integer", :default=>0
      column :created_at, "timestamp"
      column :updated_at, "timestamp"
      column :name, "varchar(255)"
      column :basis, "varchar(255)"
      column :parent_id, "integer"
    end

    create_table(:organs) do
      primary_key :id
      column :name, "varchar(255)"
    end

    create_table(:reports) do
      primary_key :id
      column :verification_id, "integer"
      column :data, "text"
      column :verification_date, "date"
      column :checked_date, "date"
    end

    create_table(:role_names) do
      primary_key :id
      column :name, "varchar(255)"
    end

    create_table(:roles) do
      primary_key :id
      column :name, "varchar(255)"
    end

    create_table(:schema_migrations) do
      column :filename, "varchar(255)", :null=>false

      primary_key [:filename]
    end

    create_table(:settings) do
      primary_key :id
      column :content, "text"
      column :name, "varchar(255)"
    end

    create_table(:tasks) do
      primary_key :id
      column :sid, "varchar(255)"
      column :name, "varchar(255)"
      column :formkey, "varchar(255)"
      column :title, "varchar(255)"
      column :descr, "varchar(255)"
      column :who, "varchar(255)"
      column :created_at, "timestamp"
      column :params, "varchar(255)"
      column :state, "varchar(255)"
    end

    create_table(:users) do
      primary_key :id
      column :email, "varchar(255)", :default=>"", :null=>false
      column :password, "varchar(255)", :default=>""
      column :encrypted_password, "varchar(255)", :default=>"", :null=>false
      column :reset_password_token, "varchar(255)"
      column :reset_password_sent_at, "varchar(255)"
      column :remember_created_at, "timestamp"
      column :sign_in_count, "integer", :default=>0, :null=>false
      column :current_sign_in_at, "timestamp"
      column :last_sign_in_at, "timestamp"
      column :current_sign_in_ip, "varchar(255)"
      column :last_sign_in_ip, "varchar(255)"
      column :created_at, "timestamp"
      column :updated_at, "timestamp"
      column :username, "varchar(255)"
      column :organ_id, "integer"
      column :role_id, "integer"

      # index [:email], :unique=>true
      # index [:reset_password_token], :unique=>true
    end

    create_table(:vendors) do
      primary_key :id
      column :name, "varchar(255)"
      column :nickname, "varchar(255)"
      column :pinyin, "varchar(255)"
      column :contact, "varchar(255)"
      column :phone, "integer"
      column :category_id, "integer"
      column :status, "integer", :default=>0
      column :created_at, "timestamp"
      column :updated_at, "timestamp"
    end

    create_table(:verifications) do
      primary_key :id
      column :sn, "varchar(255)"
      column :organ_id, "integer"
      column :date, "date"
      column :status, "integer", :default=>0
      column :state, "varchar(255)"
      column :created_at, "timestamp"
      column :updated_at, "timestamp"
      column :report_id, "integer"
      column :instrument_id, "integer"
      column :standard_id, "integer"
    end
  end
end
