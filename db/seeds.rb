# -*- coding: utf-8 -*-

#Wdy::Unirole::TestData.initdata
['common', 'management', 'admin'].each do |role|
  Role.find_or_create(name: role)
end

organs = %w(安检科 计量站 信息站 泸州采气作业区 纳溪采气作业区 合江采气作业区 江安采气作业区 自贡采气作业区 隆昌采气作业区 乐山采气作业区 渝西采气作业区 荣县净化厂 安岳油气处理厂 试修作业中心 泸州炭黑厂 勘探研究所 工艺研究所 维修抢险中心 消防大队 泸州西部公司 自贡西部公司 公共事务管理中心)
organs.each do |organ|
  Organ.find_or_create(name: organ)
end

# --------- 这3个单位默认可以查询全部仪器，其余单位人员只能查询自己单位的仪器 ----------
%w(安检科 计量站 信息站).each do |e|
  organ = Organ.where(name: e).first
  organ.update(scope: "ALL")
end



Setting.find_or_create(name: '组织结构图')

# 初始化 分类
#Category.all.map{|c| c.delete}
#CategoryType.all.map{|c| c.delete}
#CategoryRange.all.map{|c| c.delete}
#CategoryPrecision.all.map{|c| c.delete}

# 检定分类
pressure = Category.find_or_create(name: '压力')
geometry = Category.find_or_create(name: '几何')
flow = Category.find_or_create(name: '流量')
temperature = Category.find_or_create(name: '温度')
chemistry = Category.find_or_create(name: '化学')

# 压力－仪器类型
pressure_types = ['数字压力计','压力变送器','活塞式压力计','精密压力表','普通压力表']
pressure_types.each do |name|
  InstrumentKind.find_or_create(name: name, category: pressure, expries_date: 90)
end

#几何
geometry_types = ['标准孔板','常用玻璃量器','砝码','机械天平','架盘天平','电子天平']
geometry_types.each do |name|
  InstrumentKind.find_or_create(name: name, category: geometry, expries_date: 90)
end
# 气体
chemistry_types = ['硫化氢气体检测仪','可燃气体检测报警器','温度变送器']
chemistry_types.each do |name|
  InstrumentKind.find_or_create(name: name, category: chemistry, expries_date: 90)
end
# 流量
flow_types = ['速度式流量计','涡轮式流量计','涡街式流量计','气体容积式流量计']
flow_types.each do |name|
  InstrumentKind.find_or_create(name: name, category: flow, expries_date: 90)
end
# 温度
temperature_types = ['工作用廉金属热电偶','工业铂铜热电阻','工作用玻璃液体温度计','双金属温度计']
temperature_types.each do |name|
  InstrumentKind.find_or_create(name: name, category: temperature, expries_date: 90)
end

# 压力－检测量程
pressure_ranges = [
  "0-6 MPa",
  "0-200 MPa",
  "0.1-6 MPa",
  "1-60 MPa",
  "0.04-0.6 MPa"
]

# 压力－检测精度
pressure_precisions = ['0.05','0.02','0.0005','0.0002']


pressure.instrument_kinds.each do |kind|
  kind.set(regulation: pressure_ranges.join(","), accuracy: pressure_precisions.join(",")).save
end


def init_regulations category, regs
  regs.each do |regulation|
    Regulation.find_or_create(sn: regulation[0],name: regulation[1],
      published_date:regulation[2], material_date: regulation[3], category_id: category.id,
      instrument_kind_id: InstrumentKind.where(name: regulation[4]).first.id)
  end
end


init_regulations(flow, [
  ['JJG 198-1994',"《速度式流量计检定规程》","1994-05-09","1994-12-01", "速度式流量计"],
  ['JJG 1037-2008',"《涡轮流量计检定规程》","208-03-25","2008-06-25",   "涡轮式流量计"],
  ['JJG 1029-2007',"《涡街流量计检定规程》","2007-08-21","2007-11-21",  "涡街式流量计"],
  ['JJG 633-2005',"《气体容积式流量计检定规程》","2005-04-28","2005-10-28", "气体容积式流量计"]
])

init_regulations(pressure, [
  ['JJG 49-2013',"《弹性元件式精密压力表和真空表检定规程》","2013-06-27","2013-12-27", "精密压力表"],
  ['JJG 52-2013',"《弹性元件式一般压力表、压力真空表和真空表检定规程》","2013-06-28","2013-12-28", "普通压力表"],
  ['JJG 882-2004',"《压力变送器检定规程》","2004-06-04","2004-12-01", "压力变送器"],
  ['JJG 875-2005',"《数字压力计检定规程》","2005-12-20","2006-06-20", "数字压力计"],
  ['JJG 59-2007',"《活塞式压力计检定规程》","2007-06-14","2007-12-14", "活塞式压力计"]
])

init_regulations(temperature, [
  ['JJG 351-1996',"《工业用廉金属热电偶检定规程》","1996-08-23","1997-03-01", "工作用廉金属热电偶"],
  ['JJG 229-2010',"《工业铂、铜热电阻检定规程》","2010-09-06","2011-03-06",   "工业铂铜热电阻"],
  ['JJG 130-2011',"《工作用玻璃液体温度计检定规程》","2011-09-20","2012-03-20", "工作用玻璃液体温度计"],
  ['JJG 226-2001',"《双金属温度计检定规程》","2001-06-05","2001-10-01", "双金属温度计"],
])

init_regulations(geometry, [
  ['JJG 196-2006',"《常用玻璃量器检定规程》","2006-12-08","2007-06-08", "常用玻璃量器"],
  ['JJG 99-2006',"《砝码检定规程》","2006-09-06","2007-03-06", "砝码"],
  ['JJG 98-2006',"《机械天平检定规程》","2006-09-06","2007-03-06", "机械天平"],
  ['JJG 156-2004',"《架盘天平检定规程》","2004-03-02","2004-09-02", "架盘天平"],
  ['JJG 1036-2008',"《电子天平检定规程》","2008-02-20","2008-05-20", "电子天平"],
])

init_regulations(chemistry, [
  ['JJG 640-1994',"《差压式流量计检定规程》","1994-07-12","1994-12-01", "标准孔板"],
  ['JJG 695-2003',"《硫化氢气体检测仪检定规程》","2003-09-23","2004-03-23", "硫化氢气体检测仪"],
  ['JJG 693-2011',"《可燃气体检测报警器检定规程》","2011-06-14","211-12-14", "可燃气体检测报警器"],
  ['JJG 1183-2007',"《温度变送器校准规范》","2007-11-21","2008-05-21", "温度变送器"]
])


# ---------------------------------------
# 供应商举例
# ---------------------------------------
["太原精密仪器厂", "成都康德设备厂"].each do |v|
  Vendor.find_or_create(name: v, category: pressure)
end


